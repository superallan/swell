---
layout: blog
title: Dog City, Jim Henson, 1989 - Senseless Car Chase Scene
date: 2020-07-29T09:37:29.450Z
media: https://youtu.be/0JZ9-BjbXoU
context: >-
  The freedom to explore, provided by the anthology format of [THE JIM HENSON
  HOUR](https://en.wikipedia.org/wiki/The_Jim_Henson_Hour), was a gift to [Jim
  Henson](https://en.wikipedia.org/wiki/Jim_Henson), allowing him to joyfully
  realize a long-simmering concept spawned by his affection for canine
  characters: DOG CITY. 


  This [film noir](https://en.wikipedia.org/wiki/Film_noir) parody was an opportunity for Jim to revisit the silliness of his early work with a team of his closest collaborators. Long-time writing partner [Jerry Juhl](https://en.wikipedia.org/wiki/Jerry_Juhl) delighted in the delicious puns and silly jokes that litter his script while Jim tried to keep a straight face both behind and in front of the camera. 


  Cast: doggy puppet versions of Jim Henson, Kevin Clash, Fran Brill
---
