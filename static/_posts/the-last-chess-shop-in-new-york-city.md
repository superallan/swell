---
layout: blog
title: The Last Chess Shop in New York City
date: 2020-07-05T16:12:00.504Z
media: https://youtu.be/eh8U6SLvNgs
context: >-
  > I came here to get a PhD in American literature, and here I am, with
  pictures of American writers on the wall—a chess vendor.


  That’s Imad Khachan, owner of [Chess Forum](https://www.chessforum.com/), the only remaining chess shop in New York City. A Palestinian refugee with no family of his own, Khachan has become “the father of everybody” to a community of chess enthusiasts, those curious to learn more about the game, and those whom Khachan describes as the city’s “invisible people.” 


  “When no other place will welcome you, you have a seat \[here],” Khachan says in [Lonelyleap](https://lonelyleap.com/work/king-of-the-night)'s short documentary, King of the Night. The film depicts the chess shop as more than a home for chess players; Khachan’s open-door policy has provided refuge for many patrons with difficult lives at home. According to Molly Brass, one of the film’s directors, some Chess Forum regulars have no home at all. 


  “There are very few places in New York that accept and welcome anyone—really anyone—to sit and spend as many hours as they want to, at negligible or no cost,” Brass told [The Atlantic](https://en.wikipedia.org/wiki/The_Atlantic). “It's almost a revolutionary idea at this point.” "King of the Night" was produced by Lonelyleap. 


  It is part of The Atlantic Selects, an online showcase of short documentaries from independent creators, curated by [The Atlantic](https://en.wikipedia.org/wiki/The_Atlantic).
---
