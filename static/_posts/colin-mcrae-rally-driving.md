---
layout: blog
title: "Colin McRae: Rally Driving"
date: 2020-06-25T08:31:15.003Z
media: https://www.youtube.com/watch?v=ZbTbZfUKChY
context: "Colin Steele McRae,
  [MBE](https://en.wikipedia.org/wiki/Order_of_the_British_Empire \"Order of the
  British Empire\") (5 August 1968 – 15 September 2007) was a British
  [rally](https://en.wikipedia.org/wiki/Rallying \"Rallying\") driver from
  [Lanark](https://en.wikipedia.org/wiki/Lanark \"Lanark\"),
  [Scotland](https://en.wikipedia.org/wiki/Scotland \"Scotland\"). The son of
  five-time British Rally Champion [Jimmy
  McRae](https://en.wikipedia.org/wiki/Jimmy_McRae \"Jimmy McRae\") and brother
  of rally driver [Alister McRae](https://en.wikipedia.org/wiki/Alister_McRae
  \"Alister McRae\"), Colin McRae was the 1991 and 1992 [British Rally
  Champion](https://en.wikipedia.org/wiki/British_Rally_Championship \"British
  Rally Championship\"), and in
  [1995](https://en.wikipedia.org/wiki/1995_World_Rally_Championship_season
  \"1995 World Rally Championship season\") became the first Scottish person and
  the youngest to win the [World Rally
  Championship](https://en.wikipedia.org/wiki/World_Rally_Championship \"World
  Rally Championship\") [Drivers'
  title](https://en.wikipedia.org/wiki/List_of_World_Rally_Championship_Drivers\
  %27_Champions \"List of World Rally Championship Drivers' Champions\"), a
  record he still holds. With 25 victories in the WRC, McRae previously held the
  record for the most wins in the series."
---
