---
layout: blog
title: The Story of Super Mario Bros. 3
date: 2020-06-29T11:38:37.641Z
media: https://youtu.be/MxT6IwUtLSU
context: >
  A documentary about the creation of [Super Mario Bros.
  3](https://en.wikipedia.org/wiki/Super_Mario_Bros._3), the unparalleled hype
  surrounding its release, and why it's considered one of the greatest games of
  all time.
---
