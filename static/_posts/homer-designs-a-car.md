---
layout: blog
title: Homer Designs A Car
date: 2020-06-25T14:29:23.195Z
media: https://youtu.be/WPc-VEqBPHI
context: '"The Homer" (also known as "The Car Built for Homer") was an
  infamous concept car that yielded disastrous financial results for the company
  that produced it, [Powell
  Motors](https://simpsons.fandom.com/wiki/Powell_Motors "Powell Motors").
  Powell Motors CEO [Herb Powell](https://simpsons.fandom.com/wiki/Herb_Powell
  "Herb Powell") believed the company needed to create "the type of car
  [Americans](https://simpsons.fandom.com/wiki/United_States "United States")
  really want, not the kind we tell them they want" in order to "beat the
  [Japanese](https://simpsons.fandom.com/wiki/Japan "Japan")." In this endeavor,
  Herb hired his recently discovered half-brother [Homer
  Simpson](https://simpsons.fandom.com/wiki/Homer_Simpson "Homer Simpson") to
  design such a car, believing Homer to "understand the needs and wants of the
  average American car owner."'
---
