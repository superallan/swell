---
layout: blog
title: Ferragamo Loafer Restoration
date: 2020-06-29T10:30:57.202Z
media: https://youtu.be/qP74T9CwbBg
context: 'Salvatore Ferragamo (5 June 1898 – 7 August 1960) was an
  [Italian](https://en.wikipedia.org/wiki/Italy "Italy") shoe
  [designer](https://en.wikipedia.org/wiki/Fashion_designer "Fashion designer")
  and the founder of [luxury goods](https://en.wikipedia.org/wiki/Luxury_goods
  "Luxury goods") high-end retailer [Salvatore Ferragamo
  S.p.A.](https://en.wikipedia.org/wiki/Salvatore_Ferragamo_S.p.A. "Salvatore
  Ferragamo S.p.A.") One of the most innovative shoe designers of the 20th
  century, Salvatore Ferragamo, rose to fame in the 1930s. In addition to
  experimenting with unusual materials including kangaroo, crocodile, and fish
  skin, Ferragamo drew on historic inspiration for his shoes. His cork wedge
  sandals—often imitated and reimagined—are considered one of the most important
  shoe designs of the 20th century.'
---
