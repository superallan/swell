---
layout: blog
title: The Divine Comedy - Tonight We Fly
date: 2020-06-28T18:31:29.948Z
media: https://youtu.be/cnY9ea_q3nI
context: >-
  [Promenade](https://en.wikipedia.org/wiki/Promenade_(The_Divine_Comedy_album))
  is [The Divine Comedy](https://en.wikipedia.org/wiki/The_Divine_Comedy_(band)
  "The Divine Comedy (band)")'s third album.


  *Promenade* can be interpreted as a [concept album](https://en.wikipedia.org/wiki/Concept_album "Concept album") about two lovers who spend a day at the seaside. There are many different interpretations of the story, but it may run something like this: "Bath" is about the female character taking a bath, and "Going Downhill Fast" is about the male character bicycling over to her house. "The Booklovers" is about the two discussing their favourite authors. "A Seafood Song" is about them enjoying a meal comprising different types of fish. In "Geronimo" they get caught in the rain as they head back to his place, and they go on a [Ferris wheel](https://en.wikipedia.org/wiki/Ferris_wheel "Ferris wheel") ride in "Don't Look Down", where the thrill of the moment causes the man to announce his [atheism](https://en.wikipedia.org/wiki/Atheism "Atheism"). Later, in "When the Lights Go Out All Over Europe" (the title alluding to the famous [World War I](https://en.wikipedia.org/wiki/World_War_I "World War I") quote "[The lights are going out all over Europe](https://en.wikipedia.org/wiki/The_lamps_are_going_out "The lamps are going out")") they see a French film, and in "The Summerhouse" they reminisce about their childhood. The girl almost drowns during an evening stroll in "Neptune's Daughter", they then get drunk in "A Drinking Song", and "Ten Seconds to Midnight" is about counting down to the New Year and the anniversary of the time when they first met. Finally, in "Tonight We Fly", they transcend everyone through their ecstasy.
---
