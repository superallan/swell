---
layout: blog
title: Louie Barletta's "Bag of Suck" Part
date: 2020-06-26T22:01:04.675Z
media: https://youtu.be/6tD9hWKSgN8
context: >-
  Nobody skates like Louie Barletta, and this part from enjoi’s 2006 offering is
  a bonafide all-timer.\

  \

  Enjoi released its first full-length video, *Bag of Suck*, in 2006 and won the Transworld "Skate Video of the Year" award in 2007.
---
