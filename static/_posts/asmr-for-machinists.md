---
layout: blog
title: ASMR for Machinists
date: 2020-06-27T14:47:39.563Z
media: https://youtu.be/ATFrUQCTY4U
context: >-
  [ASMR](https://en.wikipedia.org/wiki/ASMR) signifies the subjective experience
  of "low-grade [euphoria](https://en.wikipedia.org/wiki/Euphoria "Euphoria")"
  characterized by "a combination of positive feelings and a distinct
  static-like tingling sensation on the skin". It is most commonly triggered by
  specific auditory or visual stimuli, and less commonly by intentional
  attention
  control. A genre
  of videos which intend to stimulate ASMR has emerged, of which over 13 million
  are published on [YouTube](https://en.wikipedia.org/wiki/YouTube
  "YouTube").


  A [machinist](https://en.wikipedia.org/wiki/Machinist) is a [tradesperson](https://en.wikipedia.org/wiki/Tradesperson "Tradesperson") who [machines](https://en.wikipedia.org/wiki/Machining "Machining") using hand tools and [machine tools](https://en.wikipedia.org/wiki/Machine_tool "Machine tool") to create or modify a part that is made of metal, plastics, or wood, although plastics or wood machining is rarely manufactured by machinists as those materials require lesser skill and almost all the work performed by a machinist is done on metal. A machinist is to metal what a carpenter or cabinet maker is to wood.
---
