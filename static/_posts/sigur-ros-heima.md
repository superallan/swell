---
layout: blog
title: Sigur Ros - Heima
date: 2020-07-05T15:56:34.669Z
media: https://youtu.be/afxiUrfhF8A
context: >-
  > This is a re-cut, extended and very different version of the [Sigur
  Rós](https://en.wikipedia.org/wiki/Sigur_R%C3%B3s) tour movie
  [Heima](https://en.wikipedia.org/wiki/Heima). It's mostly made up of material
  from the 2 disc version DVD. The result is full, uninterupted performaces,
  beautiful landscapes and not much more. Straight to the core. No interviews,
  voice-overs or other distracting stuff. Most things cut out are both good and
  interesting but without a place in this particular version.


  \

  [Sigur Rós](https://en.wikipedia.org/wiki/Sigur_R%C3%B3s) is an Icelandic [post-rock](https://en.wikipedia.org/wiki/Post-rock "Post-rock") band from [Reykjavík](https://en.wikipedia.org/wiki/Reykjav%C3%ADk "Reykjavík"), active since 1994. Known for their ethereal sound, frontman [Jónsi](https://en.wikipedia.org/wiki/J%C3%B3nsi "Jónsi")'s [falsetto](https://en.wikipedia.org/wiki/Falsetto "Falsetto") vocals, and the use of [bowed guitar](https://en.wikipedia.org/wiki/Bowed_guitar "Bowed guitar"), the band's music incorporates [classical](https://en.wikipedia.org/wiki/Classical_music "Classical music") and [minimal](https://en.wikipedia.org/wiki/Minimal_music "Minimal music") aesthetic elements. 


  [Heima](https://en.wikipedia.org/wiki/Heima) (*at home*) is a documentary film and double DVD set about the tour around [Iceland](https://en.wikipedia.org/wiki/Iceland "Iceland") in the summer of 2006. During the tour the band played two big open-air concerts at Miklatún - [Reykjavík](https://en.wikipedia.org/wiki/Reykjav%C3%ADk "Reykjavík") (30 July) and [Ásbyrgi](https://en.wikipedia.org/wiki/%C3%81sbyrgi "Ásbyrgi") (4 August), as well as small scale concerts at [Ólafsvík](https://en.wikipedia.org/wiki/%C3%93lafsv%C3%ADk "Ólafsvík") (24 July), [Ísafjörður](https://en.wikipedia.org/wiki/%C3%8Dsafj%C3%B6r%C3%B0ur "Ísafjörður") (26 July), [Djúpavík](https://en.wikipedia.org/wiki/Dj%C3%BApav%C3%ADk "Djúpavík") (27 July), [Háls](https://en.wikipedia.org/w/index.php?title=H%C3%A1ls&action=edit&redlink=1 "Háls (page does not exist)"), [Öxnadalur](https://en.wikipedia.org/w/index.php?title=%C3%96xnadalur&action=edit&redlink=1 "Öxnadalur (page does not exist)") (28 July) and [Seyðisfjörður](https://en.wikipedia.org/wiki/Sey%C3%B0isfj%C3%B6r%C3%B0ur "Seyðisfjörður") (3 August). In addition, a protest concert against the [Kárahnjúkar dam](https://en.wikipedia.org/wiki/K%C3%A1rahnj%C3%BAkar_Hydropower_Plant "Kárahnjúkar Hydropower Plant") was performed at [Snæfellsskála](https://en.wikipedia.org/w/index.php?title=Sn%C3%A6fellssk%C3%A1la&action=edit&redlink=1 "Snæfellsskála (page does not exist)") (3 August). The documentary also includes footage of an acoustic concert played for family and friends at Gamla Borg, a coffee shop in the small town Borg, on 22 April 2007.
---
