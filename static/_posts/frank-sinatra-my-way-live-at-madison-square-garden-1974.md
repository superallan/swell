---
layout: blog
title: Frank Sinatra - My Way (Live At Madison Square Garden, 1974)
date: 2020-07-25T07:19:22.089Z
media: https://www.youtube.com/watch?v=w019MzRosmk
context: >-
  [Francis Albert "Frank" Sinatra](https://en.wikipedia.org/wiki/Frank_Sinatra)
  (December 12, 1915 – May 14, 1998) was an American singer, musician, actor and
  filmmaker. His singing career was 60 years long, and more than 250 million
  records of his have been sold worldwide. Extremely regarded as one of the best
  and most acclaimed popular singers in history.


  He is also well known by the nickname "Old Blue Eyes". *[The New York Times](https://simple.wikipedia.org/wiki/The_New_York_Times "The New York Times")* said he was "the first modern pop superstar". At first, he was mostly known as a *[crooner](https://simple.wikipedia.org/w/index.php?title=Crooner&action=edit&redlink=1 "Crooner (not yet started)")*, a singer of love songs. By the 1950s and 1960s, he was singing [swing](https://simple.wikipedia.org/w/index.php?title=Swing&action=edit&redlink=1 "Swing (not yet started)") and [jazz](https://simple.wikipedia.org/wiki/Jazz "Jazz") songs as well. Sinatra was also part of the Rat Pack, a group of [entertainers](https://simple.wikipedia.org/wiki/Entertainment "Entertainment") (musicians and actors), in the 1950s and 1960s. The name was informal, and the group was not an official organization of any sort, but a group of friends. Members of the Rat Pack included Sinatra, [Dean Martin](https://simple.wikipedia.org/wiki/Dean_Martin "Dean Martin"), [Sammy Davis, Jr.](https://simple.wikipedia.org/wiki/Sammy_Davis,_Jr. "Sammy Davis, Jr."), [Peter Lawford](https://simple.wikipedia.org/wiki/Peter_Lawford "Peter Lawford"), and [Joey Bishop](https://simple.wikipedia.org/wiki/Joey_Bishop "Joey Bishop"), as well as (more loosely) [Humphrey Bogart](https://simple.wikipedia.org/wiki/Humphrey_Bogart "Humphrey Bogart"), [Judy Garland](https://simple.wikipedia.org/wiki/Judy_Garland "Judy Garland"), [Lauren Bacall](https://simple.wikipedia.org/wiki/Lauren_Bacall "Lauren Bacall"), [Sid Luft](https://simple.wikipedia.org/w/index.php?title=Sid_Luft&action=edit&redlink=1 "Sid Luft (not yet started)"), and [Shirley MacLaine](https://simple.wikipedia.org/wiki/Shirley_MacLaine "Shirley MacLaine").


  In October 1974 he appeared at New York City's [Madison Square Garden](https://en.wikipedia.org/wiki/Madison_Square_Garden "Madison Square Garden") in a televised concert that was later released as an album under the title *[The Main Event – Live](https://en.wikipedia.org/wiki/The_Main_Event_%E2%80%93_Live "The Main Event – Live")*.
---
