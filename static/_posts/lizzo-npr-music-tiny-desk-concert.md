---
layout: blog
title: "Lizzo: NPR Music Tiny Desk Concert"
date: 2020-06-29T08:57:01.398Z
media: https://youtu.be/DFiLdByWIDY
context: >-
  Melissa Viviane Jefferson (born April 27, 1988), known professionally as
  [Lizzo](https://en.wikipedia.org/wiki/Lizzo), is an American singer, rapper,
  songwriter, and
  flutist. Born in
  [Detroit](https://en.wikipedia.org/wiki/Detroit "Detroit"),
  [Michigan](https://en.wikipedia.org/wiki/Michigan "Michigan"), she moved to
  [Houston](https://en.wikipedia.org/wiki/Houston "Houston"),
  [Texas](https://en.wikipedia.org/wiki/Texas "Texas"), where she began
  performing, before moving to
  [Minneapolis](https://en.wikipedia.org/wiki/Minneapolis "Minneapolis"), where
  she began her recording career. Before signing with Nice Life and [Atlantic
  Records](https://en.wikipedia.org/wiki/Atlantic_Records "Atlantic Records"),
  Lizzo released two studio
  albums—*[Lizzobangers](https://en.wikipedia.org/wiki/Lizzobangers
  "Lizzobangers")* (2013), and *[Big Grrrl Small
  World](https://en.wikipedia.org/wiki/Big_Grrrl_Small_World "Big Grrrl Small
  World")* (2015). Lizzo's first major-label
  [EP](https://en.wikipedia.org/wiki/Extended_play "Extended play"), *[Coconut
  Oil](https://en.wikipedia.org/wiki/Coconut_Oil_(EP) "Coconut Oil (EP)")*, was
  released in 2016.


  [Tiny Desk Concerts](https://en.wikipedia.org/wiki/Tiny_Desk_Concerts) is a video series of live concerts hosted by [NPR Music](https://en.wikipedia.org/wiki/NPR_Music "NPR Music") at the desk of *[All Songs Considered](https://en.wikipedia.org/wiki/All_Songs_Considered "All Songs Considered")* host [Bob Boilen](https://en.wikipedia.org/wiki/Bob_Boilen "Bob Boilen") in [Washington, D.C.](https://en.wikipedia.org/wiki/Washington,_D.C. "Washington, D.C.")
---
