---
layout: blog
title: Water Cremation
date: 2020-07-01T08:31:06.487Z
media: https://youtu.be/G9HMKF_sFV8
context: Within the last few years, some funeral directors have invested in more
  eco-friendly alternatives to traditional burials and flame cremations. One of
  these methods is a process known as Alkaline Hydrolysis, or water cremation.
  [Resomation](https://en.wikipedia.org/wiki/Alkaline_hydrolysis_(body_disposal))
  is an alternative to cremation and casket burial that is eco-friendly.
---
