---
layout: blog
title: Winnebago Heli-home
date: 2020-07-15T11:23:20.139Z
media: https://youtu.be/MJRlQrN8RAY
context: >-
  In 1976, Orlando Helicopter Airways in cooperation with
  [Winnebago](https://en.wikipedia.org/wiki/Winnebago_Industries) Industries
  marketed the Winnebago Heli-home, built on a
  [Sikorsky](https://en.wikipedia.org/wiki/Sikorsky_Aircraft) S-55 or S-58
  helicopter.


  The S-55 and S-58 were first put into service during the Korean War and saw service in Vietnam as well, before they were relegated to service in the reserves and the U.S. Coast Guard. Gas powered, these venerable helicopters were well suited to become the flying carpets for the RV set, even though few could afford the $300,000.00 price tag at the time.


  These were fully functional and well equipped Winnebagos. Also sold under the Itasca name plate, these go anywhere RVs had bathrooms with hot showers and toilets, a full galley with stove and refrigerator, wet bar, color TV, stereo, awning with optional screen room, on board communications system, 3.5 KW generator, fresh water tank, two holding tanks, 6,000 BTU air conditioner, 40,000 BTU furnace and sleeping for up to six.


  The Heli-home had all the necessary communications gear and avionics available for the time, including an onboard communications system for use while in flight.


  A large sliding patio door on the right side opened up onto your ‘patio’ which was covered by a box type manual awning.


  Don’t worry about landing next to the lake, by the way. Optional pontoons could be purchased for water landings, making the Heli-home a flying house boat too. This would have been quite useful for wilderness landings as the rotor diameter was 56 feet, so you needed a fair amount of space to land.


  Powered by a gas engine, turbine engines by Pratt and Whitney could be installed for more power and efficiency.


  The heli-home weighed in at about 9,200 pounds and had a cargo carrying capacity of 3,300 pounds, which was better than many RVs have now.


  With a top speed of about 110 mph, you could fly three and a half hours with a full 220 gallon fuel tank. This would have been enough to get you out to a wilderness area from just about any urban area in the country, but frequent fuel hops would still be required for long distance travel.


  The Winnebago Heli-home was not a commercial success. There were only eight conversions done, and none of the conversions still exist. The birds that were converted are still around, including the two that were used in the marketing of the Heli-home.
---
