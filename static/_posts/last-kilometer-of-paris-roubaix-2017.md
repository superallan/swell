---
layout: blog
title: Last kilometer of Paris-Roubaix 2017
date: 2020-06-29T08:31:03.166Z
media: https://youtu.be/O-3OuhN7u9g
context: >-
  The [Paris–Roubaix](https://en.wikipedia.org/wiki/Paris–Roubaix) is a one-day
  professional men's [bicycle road
  race](https://en.wikipedia.org/wiki/Road_bicycle_racing "Road bicycle racing")
  in northern [France](https://en.wikipedia.org/wiki/France "France"), starting
  north of [Paris](https://en.wikipedia.org/wiki/Paris "Paris") and finishing in
  [Roubaix](https://en.wikipedia.org/wiki/Roubaix "Roubaix"), at the border with
  [Belgium](https://en.wikipedia.org/wiki/Belgium "Belgium"). It is one of
  cycling's oldest races, and is one of the
  '[Monuments](https://en.wikipedia.org/wiki/Cycling_monument "Cycling
  monument")' or classics of the European calendar, and contributes points
  towards the [UCI World
  Ranking](https://en.wikipedia.org/wiki/UCI_World_Ranking "UCI World Ranking").


  The finish of the race will often be highly tactical with riders pedalling slowly, as they carefully jockey for position, often trying to force their opponents up high on the track in an attempt to get their rivals to make the first move. 


  The Paris–Roubaix is famous for rough terrain and cobblestones, or pavé ([setts](https://en.wikipedia.org/wiki/Sett_(paving) "Sett (paving)")). It has been called *the Hell of the North*, *a Sunday in Hell* (also the title of [a film](https://en.wikipedia.org/wiki/A_Sunday_in_Hell "A Sunday in Hell") about the 1976 race), *the Queen of the Classics* or *la Pascale*: the *Easter race*. Since 1977, the winner of Paris–Roubaix has received a sett (cobble stone) as part of his prize.


  The terrain has led to the development of specialised [frames](https://en.wikipedia.org/wiki/Bicycle_frame "Bicycle frame"), wheels and tyres. Punctures and other mechanical problems are common and often influence the result. Despite the esteem of the race, some cyclists dismiss it because of its difficult conditions. The race has also seen several controversies, with winners disqualified.


  From its beginning in 1896 until 1967 it started in [Paris](https://en.wikipedia.org/wiki/Paris "Paris") and ended in [Roubaix](https://en.wikipedia.org/wiki/Roubaix "Roubaix"); in 1966 the start moved to [Chantilly](https://en.wikipedia.org/wiki/Chantilly,_Oise "Chantilly, Oise"); and since 1977 it has started in [Compiègne](https://en.wikipedia.org/wiki/Compi%C3%A8gne "Compiègne"), about 85 kilometres (53 mi) north-east of the centre of Paris. Since 1943, the finish has for the most part taken place in the [Roubaix Velodrome](https://en.wikipedia.org/wiki/Roubaix_Velodrome).
---
