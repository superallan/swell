---
layout: blog
title: Non-Euclidean Worlds Game Engine
date: 2020-07-07T14:46:18.825Z
media: https://youtu.be/kEB11PQ9Eo8
context: >-
  [Non-Euclidean
  geometry](https://simple.wikipedia.org/wiki/Non-Euclidean_geometry) is a type
  of [geometry](https://simple.wikipedia.org/wiki/Geometry "Geometry").
  Non-Euclidean geometry only uses some of the
  "[postulates](https://simple.wikipedia.org/wiki/Postulate "Postulate")"
  ([assumptions](https://simple.wiktionary.org/wiki/assume "wikt:assume")) that
  [Euclidean geometry](https://simple.wikipedia.org/wiki/Euclidean_geometry
  "Euclidean geometry") is based on. In normal geometry,
  [parallel](https://simple.wikipedia.org/wiki/Parallel_(geometry) "Parallel
  (geometry)") lines can never meet. In non-Euclidean geometry they can meet,
  either infinitely many times ([elliptic
  geometry](https://simple.wikipedia.org/w/index.php?title=Elliptic_geometry&action=edit&redlink=1
  "Elliptic geometry (not yet started)")), or never ([hyperbolic
  geometry](https://simple.wikipedia.org/wiki/Hyperbolic_geometry "Hyperbolic
  geometry")).


  An example of Non-Euclidian geometry can be seen by drawing lines on a ball or other round object, straight lines that are parallel at the equator can meet at the poles.
---
