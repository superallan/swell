---
layout: blog
title: Linda Sjödin VS New Base Line V14/8B+
date: 2020-07-01T14:47:31.331Z
media: https://youtu.be/_vOWHO1zncc
context: "26-year-old Swedish climber [Linda
  Sjödin](https://www.ukclimbing.com/news/2019/09/new_base_line_8b+_by_linda_sj\
  odin-72064) has ticked Bernd Zangerl's New Base Line
  [8B+](https://en.wikipedia.org/wiki/Bouldering#Grading) in Magic Wood,
  Switzerland. The problem had seen four previous ascents by women: Shauna
  Coxsey, Anna Stöhr, Alex Puccio and Mile Heyden. Compared to the first three
  [IFSC](https://en.wikipedia.org/wiki/International_Federation_of_Sport_Climbi\
  ng) circuit legends and regulars, though, Linda is relatively unknown - a fact
  that initially dissuaded her from attempting the boulder. She is only the 11th
  woman in the world to have bouldered 8B+."
---
