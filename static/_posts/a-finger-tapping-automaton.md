---
layout: blog
title: A Finger Tapping Automaton
date: 2020-06-27T14:53:42.183Z
media: https://youtu.be/l-fcfGwepog
context: >-
  An [automaton](https://en.wikipedia.org/wiki/Automaton) is a relatively
  self-operating [machine](https://en.wikipedia.org/wiki/Machine "Machine"), or
  a machine or control mechanism designed to automatically follow a
  predetermined sequence of operations, or respond to predetermined
  instructions.Some
  automata, such as
  [bellstrikers](https://en.wikipedia.org/wiki/Jacquemart_(bellstriker)
  "Jacquemart (bellstriker)") in mechanical clocks, are designed to give the
  illusion to the casual observer that they are operating under their own
  power. 


  Since long ago the term is commonly associated with automated puppets that resemble moving humans or animals, built to impress and/or to entertain people.
---
