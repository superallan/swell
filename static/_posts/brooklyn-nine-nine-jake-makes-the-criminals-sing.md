---
layout: blog
title: Brooklyn Nine-Nine - Jake Makes the Criminals Sing
date: 2020-06-28T18:42:08.763Z
media: https://youtu.be/ffyKY3Dj5ZE
context: "[Brooklyn Nine-Nine](https://en.wikipedia.org/wiki/Brooklyn_Nine-Nine)
  is an American [police
  procedural](https://en.wikipedia.org/wiki/Police_procedural \"Police
  procedural\") [comedy](https://en.wikipedia.org/wiki/Comedy \"Comedy\")
  television series created by [Dan Goor](https://en.wikipedia.org/wiki/Dan_Goor
  \"Dan Goor\") and [Michael Schur](https://en.wikipedia.org/wiki/Michael_Schur
  \"Michael Schur\"). The series revolves around Jake Peralta ([Andy
  Samberg](https://en.wikipedia.org/wiki/Andy_Samberg \"Andy Samberg\")), an
  immature but talented
  [NYPD](https://en.wikipedia.org/wiki/New_York_Police_Department \"New York
  Police Department\") detective in
  [Brooklyn](https://en.wikipedia.org/wiki/Brooklyn \"Brooklyn\")'s fictional
  99th Precinct, who often comes into conflict with his new commanding officer,
  the serious and stern Captain Raymond Holt ([Andre
  Braugher](https://en.wikipedia.org/wiki/Andre_Braugher \"Andre Braugher\"))."
---
