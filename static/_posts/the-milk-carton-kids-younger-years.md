---
layout: blog
title: The Milk Carton Kids - Younger Years
date: 2020-07-01T15:38:17.619Z
media: https://youtu.be/mKyGwjmriaA
context: >-
  [The Milk Carton Kids](https://en.wikipedia.org/wiki/The_Milk_Carton_Kids) are
  an American [indie folk](https://en.wikipedia.org/wiki/Indie_folk "Indie
  folk") duo from [Eagle
  Rock](https://en.wikipedia.org/wiki/Eagle_Rock,_Los_Angeles "Eagle Rock, Los
  Angeles"), [California](https://en.wikipedia.org/wiki/California
  "California"), United States, consisting of singers and guitarists Kenneth
  Pattengale and Joey Ryan, who began making music together in early 2011. 


  The band has recorded and released six albums: *Retrospect*, *Prologue*, *The Ash & Clay*, *Monterey*, *All the Things That I Did and All the Things That I Didn't Do*, and *The Only Ones*. They are noted for releasing their first two albums free of charge.
---
