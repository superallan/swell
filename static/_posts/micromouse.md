---
layout: blog
title: Micromouse
date: 2020-07-10T15:33:04.328Z
media: https://youtu.be/2V6QE0GJ-zw
context: >-
  [Micromouse](https://en.wikipedia.org/wiki/Micromouse) is an event where small
  [robot](https://en.wikipedia.org/wiki/Robot "Robot")
  [mice](https://en.wikipedia.org/wiki/Mouse "Mouse") solve a 16×16
  [maze](https://en.wikipedia.org/wiki/Maze "Maze"). It began in the late
  1970s. Events are held
  worldwide, and are most popular in the
  [UK](https://en.wikipedia.org/wiki/United_Kingdom "United Kingdom"),
  [U.S.](https://en.wikipedia.org/wiki/United_States "United States"),
  [Japan](https://en.wikipedia.org/wiki/Japan "Japan"),
  [Singapore](https://en.wikipedia.org/wiki/Singapore "Singapore"),
  [India](https://en.wikipedia.org/wiki/India "India"), [South
  Korea](https://en.wikipedia.org/wiki/South_Korea "South Korea") and becoming
  popular in subcontinent countries such as [Sri
  Lanka](https://en.wikipedia.org/wiki/Sri_Lanka "Sri Lanka").


  The maze is made up of a 16×16 grid of cells, each 180 mm square with walls 50 mm high. The mice are completely [autonomous robots](https://en.wikipedia.org/wiki/Autonomous_robot "Autonomous robot") that must find their way from a predetermined starting position to the central area of the maze unaided. The mouse needs to keep track of where it is, discover walls as it explores, map out the maze and detect when it has reached the goal. Having reached the goal, the mouse will typically perform additional searches of the maze until it has found an optimal route from the start to the finish. Once the optimal route has been found, the mouse will run that route in the shortest possible time.


  Competitions and conferences are still run regularly.
---
