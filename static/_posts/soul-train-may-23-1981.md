---
layout: blog
title: Soul Train May 23, 1981
date: 2020-06-25T13:14:16.088Z
media: https://youtu.be/bebplaBOvWw
context: '*Soul Train* is an American music-dance television program which
  aired in [syndication](https://en.wikipedia.org/wiki/Broadcast_syndication
  "Broadcast syndication") from October 2, 1971, to March 27, 2006. In its
  35-year history, the show primarily featured performances by R&B, soul,
  dance/pop, and hip hop artists, although funk, jazz, disco, and gospel artists
  also appeared. The series was created by [Don
  Cornelius](https://en.wikipedia.org/wiki/Don_Cornelius "Don Cornelius"), who
  also served as its first host and executive producer.'
---
