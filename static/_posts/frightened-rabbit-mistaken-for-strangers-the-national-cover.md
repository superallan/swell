---
layout: blog
title: Frightened Rabbit - Mistaken for Strangers (The National cover)
date: 2020-06-28T18:43:51.466Z
media: https://youtu.be/NAePxA0nmO0
context: >-
  [Frightened Rabbit](https://en.wikipedia.org/wiki/Frightened_Rabbit) were a
  Scottish [indie rock](https://en.wikipedia.org/wiki/Indie_rock "Indie rock")
  band from [Selkirk](https://en.wikipedia.org/wiki/Selkirk,_Scottish_Borders
  "Selkirk, Scottish Borders"), formed in 2003. Initially a solo project for
  vocalist and guitarist [Scott
  Hutchison](https://en.wikipedia.org/wiki/Scott_Hutchison "Scott Hutchison"),
  the final line-up of the band consisted of Hutchison, [Grant
  Hutchison](https://en.wikipedia.org/wiki/Grant_Hutchison "Grant Hutchison")
  (drums), Billy Kennedy (guitar, bass), Andy Monaghan (guitar, keyboards) and
  Simon Liddell (guitar). From 2004 the band were based in
  [Glasgow](https://en.wikipedia.org/wiki/Glasgow "Glasgow").


  [The National](https://en.wikipedia.org/wiki/The_National_(band)) is an American [rock](https://en.wikipedia.org/wiki/Rock_music "Rock music") band from [Cincinnati](https://en.wikipedia.org/wiki/Cincinnati "Cincinnati"), [Ohio](https://en.wikipedia.org/wiki/Ohio "Ohio"), formed in 1999. The band consists of [Matt Berninger](https://en.wikipedia.org/wiki/Matt_Berninger "Matt Berninger") (vocals), [Aaron Dessner](https://en.wikipedia.org/wiki/Aaron_Dessner "Aaron Dessner") (guitar, piano, keyboards), [Bryce Dessner](https://en.wikipedia.org/wiki/Bryce_Dessner "Bryce Dessner")(guitar, piano, keyboards), [Scott Devendorf](https://en.wikipedia.org/wiki/Scott_Devendorf "Scott Devendorf") (bass) and [Bryan Devendorf](https://en.wikipedia.org/wiki/Bryan_Devendorf "Bryan Devendorf") (drums).
---
