---
layout: blog
title: Frightened Rabbit - Square 9
date: 2020-06-28T18:34:59.816Z
media: https://youtu.be/hvechJz7lvk
context: >-
  [Frightened Rabbit](https://en.wikipedia.org/wiki/Frightened_Rabbit) were a
  Scottish [indie rock](https://en.wikipedia.org/wiki/Indie_rock "Indie rock")
  band from [Selkirk](https://en.wikipedia.org/wiki/Selkirk,_Scottish_Borders
  "Selkirk, Scottish Borders"), formed in 2003. Initially a solo project for
  vocalist and guitarist [Scott
  Hutchison](https://en.wikipedia.org/wiki/Scott_Hutchison "Scott Hutchison"),
  the final line-up of the band consisted of Hutchison, [Grant
  Hutchison](https://en.wikipedia.org/wiki/Grant_Hutchison "Grant Hutchison")
  (drums), Billy Kennedy (guitar, bass), Andy Monaghan (guitar, keyboards) and
  Simon Liddell (guitar). From 2004 the band were based in
  [Glasgow](https://en.wikipedia.org/wiki/Glasgow
  "Glasgow").


  Hutchison disappeared on 9 May 2018, and his body was found the following day on the banks of the [Firth of Forth](https://en.wikipedia.org/wiki/Firth_of_Forth "Firth of Forth").
---
