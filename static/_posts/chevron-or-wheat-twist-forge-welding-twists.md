---
layout: blog
title: Chevron or Wheat twist, forge welding twists
date: 2020-06-27T14:55:23.024Z
media: https://youtu.be/c80mMzDknTI
context: When you have ability to [forge
  weld](https://en.wikipedia.org/wiki/Forge_welding), it opens up all sorts of
  possibilities in your forged iron work. Things like this wheat or chevron
  twist rely on forge welding the component part both before and after the
  twist, it opens up all sorts of possibilities in your forged iron work. Things
  like this wheat or chevron twist rely on forge welding the component part both
  before and after the twist.
---
