---
layout: blog
title: Toto - Africa (Playing in an empty shopping centre)
date: 2020-07-29T12:15:50.326Z
media: https://youtu.be/D__6hwqjZAs
context: >-
  **[Mallwave](https://knowyourmeme.com/memes/cultures/mallwave-mallsoft)**,
  also known as
  **[Mallsoft](https://knowyourmeme.com/memes/cultures/mallwave-mallsoft)**, is
  a subgenre of [Vaporwave](https://knowyourmeme.com/memes/cultures/vaporwave)
  music meant to elicit nostalgia using imagery of shopping centers and remixed
  anonymous soft rock muzak one might hear in a shopping mall.


  "**Africa**" is a song recorded by the American rock band [Toto](https://en.wikipedia.org/wiki/Toto_(band) "Toto (band)") in 1981, for their fourth studio album *[Toto IV](https://en.wikipedia.org/wiki/Toto_IV "Toto IV"),* and released as the album's third single on September 30, 1982, through [Columbia Records](https://en.wikipedia.org/wiki/Columbia_Records "Columbia Records"). The song was written by band members [David Paich](https://en.wikipedia.org/wiki/David_Paich "David Paich") and [Jeff Porcaro](https://en.wikipedia.org/wiki/Jeff_Porcaro "Jeff Porcaro").



  The song was popular upon its release, hitting number one on the [*Billboard* Hot 100](https://en.wikipedia.org/wiki/Billboard_Hot_100 "Billboard Hot 100") in February 1983, and the song has continued to be a popular soft-rock classic into the 21st century. The song has been utilized in many [internet memes](https://en.wikipedia.org/wiki/Internet_meme "Internet meme"), has appeared in television shows, such as *[Stranger Things](https://en.wikipedia.org/wiki/Stranger_Things "Stranger Things")*, *[Family Guy](https://en.wikipedia.org/wiki/Family_Guy "Family Guy")*, *[Chuck](https://en.wikipedia.org/wiki/Chuck_Versus_the_Best_Friend "Chuck Versus the Best Friend")*, and *[South Park](https://en.wikipedia.org/wiki/South_Park "South Park")*, and was used by [CBS](https://en.wikipedia.org/wiki/CBS "CBS") during their coverage of the [funeral](https://en.wikipedia.org/wiki/Death_of_Nelson_Mandela "Death of Nelson Mandela") of former [South African President](https://en.wikipedia.org/wiki/South_African_President "South African President") [Nelson Mandela](https://en.wikipedia.org/wiki/Nelson_Mandela "Nelson Mandela"), albeit not without controversy.


  In 2012, "Africa" was listed by music magazine *[NME](https://en.wikipedia.org/wiki/NME "NME")* in 32nd place on its list of "50 Most Explosive Choruses."


  In January 2019, [a sound installation was set up in an undisclosed location](https://www.bbc.co.uk/news/world-africa-46861137) in the [Namib Desert](https://en.wikipedia.org/wiki/Namib "Namib") to play the song on a constant loop. The installation is powered by solar batteries, allowing the song to be played indefinitely.
---
