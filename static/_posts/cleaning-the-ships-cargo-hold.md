---
layout: blog
title: Cleaning the Ship's Cargo Hold
date: 2020-06-29T10:40:30.924Z
media: https://youtu.be/is4cqxLM-N4
context: A ship's main purpose is to [transport
  cargo](https://en.wikipedia.org/wiki/Maritime_transport), so naturally one of
  the most important tasks is to make sure that the cargo holds are clean and
  ready to receive cargo.
---
