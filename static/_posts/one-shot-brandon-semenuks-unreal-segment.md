---
layout: blog
title: "One Shot: Brandon Semenuk's unReal Segment"
date: 2020-06-27T12:15:41.242Z
media: https://youtu.be/5ud5T5I4XcA
context: >-
  [Brandon Semenuk](https://en.wikipedia.org/wiki/Brandon_Semenuk) is a
  [Canadian](https://en.wikipedia.org/wiki/Canadians "Canadians")
  [freeride](https://en.wikipedia.org/wiki/Freeride "Freeride") [mountain
  biker](https://en.wikipedia.org/wiki/Mountain_biker "Mountain biker") from
  [Whistler, British
  Columbia](https://en.wikipedia.org/wiki/Whistler,_British_Columbia "Whistler,
  British Columbia"). Semenuk is a 3-time FMB World Tour Gold Medalist. He is a
  2013 Munich [X-Games](https://en.wikipedia.org/wiki/X-Games "X-Games") Silver
  medalist in the Mountain Bike Slopestyle event, a 2008, 2016 and 2019 [Red
  Bull Rampage](https://en.wikipedia.org/wiki/Red_Bull_Rampage "Red Bull
  Rampage") winner, 2nd person who won rampage three-times.


  Widely recognized as the best slopestyle mountain biker in the world, Brandon Semenuk has become the first mountain biker to film a full segment in a single continuous shot. This uninterrupted shot was filmed by the most advanced gyro-stabilized camera system, the GSS C520, mounted to a truck on a custom road built next to a custom trail that took three weeks to build. This true cinematic achievement required perfect coordination between Semenuk, Anthill Films, and TGR with regard to athleticism, planning, timing and logistics. 


  Injured at the time, Semenuk only hit the full line once.
---
