---
layout: blog
title: Basic Knife Skills
date: 2020-07-01T08:23:11.919Z
media: https://www.youtube.com/watch?v=G-Fg7l7G1zw
context: If you're doing a lot of food prep at home, or if you're heading out to
  volunteer at a soup kitchen, it pays to know how to properly use a kitchen
  knife. This video covers the basics.
---
