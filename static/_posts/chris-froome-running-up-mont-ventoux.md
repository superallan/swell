---
layout: blog
title: Chris Froome running up Mont-Ventoux
date: 2020-06-29T08:04:09.895Z
media: https://youtu.be/UKpa3hYjeGo
context: >-
  Britain's [Chris Froome](https://en.wikipedia.org/wiki/Chris_Froome) was
  forced to run without a bike following a crash with a motorbike on one of the
  most iconic climbs of the [Tour de
  France](https://en.wikipedia.org/wiki/Tour_de_France).


  Froome crashed with [Richie Porte](https://en.wikipedia.org/wiki/Richie_Porte) and [Bauke Mollema](https://en.wikipedia.org/wiki/Bauke_Mollema) just over one kilometre from the finish on [Mont Ventoux](https://en.wikipedia.org/wiki/Mont_Ventoux). He was overtaken by rivals [Adam Yates](https://en.wikipedia.org/wiki/Adam_Yates_(cyclist)) and [Nairo Quintana](https://en.wikipedia.org/wiki/Nairo_Quintana) in the melee but race organisers ruled Froome should retain the overall lead.\

  \

  Chris Froome, [OBE](https://en.wikipedia.org/wiki/Officer_of_the_Order_of_the_British_Empire "Officer of the Order of the British Empire") (born 20 May 1985) is a British [road racing cyclist](https://en.wikipedia.org/wiki/Road_bicycle_racing "Road bicycle racing"), who currently rides for [UCI WorldTeam](https://en.wikipedia.org/wiki/UCI_WorldTeam "UCI WorldTeam") [Team Ineos](https://en.wikipedia.org/wiki/Team_Ineos "Team Ineos"). He has won several [stage races](https://en.wikipedia.org/wiki/Road_bicycle_racing#Stage_races "Road bicycle racing"), including four editions of the [Tour de France](https://en.wikipedia.org/wiki/Tour_de_France "Tour de France") (in [2013](https://en.wikipedia.org/wiki/2013_Tour_de_France "2013 Tour de France"), [2015](https://en.wikipedia.org/wiki/2015_Tour_de_France "2015 Tour de France"), [2016](https://en.wikipedia.org/wiki/2016_Tour_de_France "2016 Tour de France") and [2017](https://en.wikipedia.org/wiki/2017_Tour_de_France "2017 Tour de France")), the [Giro d'Italia](https://en.wikipedia.org/wiki/Giro_d%27Italia "Giro d'Italia") in [2018](https://en.wikipedia.org/wiki/2018_Giro_d%27Italia "2018 Giro d'Italia") and the [Vuelta a España](https://en.wikipedia.org/wiki/Vuelta_a_Espa%C3%B1a "Vuelta a España") twice ([2011](https://en.wikipedia.org/wiki/2011_Vuelta_a_Espa%C3%B1a "2011 Vuelta a España") and [2017](https://en.wikipedia.org/wiki/2017_Vuelta_a_Espa%C3%B1a "2017 Vuelta a España")). Froome has also won two Olympic bronze medals in [road time trials](https://en.wikipedia.org/wiki/Individual_time_trial "Individual time trial"), in [2012](https://en.wikipedia.org/wiki/Cycling_at_the_2012_Summer_Olympics_%E2%80%93_Men%27s_road_time_trial "Cycling at the 2012 Summer Olympics – Men's road time trial") and [2016](https://en.wikipedia.org/wiki/Cycling_at_the_2016_Summer_Olympics_%E2%80%93_Men%27s_road_time_trial "Cycling at the 2016 Summer Olympics – Men's road time trial"), and also took bronze in the [2017 World Championships](https://en.wikipedia.org/wiki/2017_UCI_Road_World_Championships_%E2%80%93_Men%27s_time_trial "2017 UCI Road World Championships – Men's time trial").
---
