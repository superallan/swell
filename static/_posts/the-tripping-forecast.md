---
layout: blog
title: The Tripping Forecast
date: 2020-06-29T16:17:52.342Z
media: https://youtu.be/CruFRlPERWE
context: >-
  Listen to the complete Tripping Forecast on
  [SoundCloud](https://soundcloud.com/l_o_n_g_w_a_v_e/the-tripping-forecast)


  Inspired by [Chill Out](https://en.wikipedia.org/wiki/Chill_Out_(KLF_album)), released by [The KLF](https://en.wikipedia.org/wiki/The_KLF "The KLF") in February 1990, is one of the earliest [ambient house](https://en.wikipedia.org/wiki/Ambient_house "Ambient house") concept albums. The music portrays a mythical night-time journey to the U.S. [Gulf Coast](https://en.wikipedia.org/wiki/Gulf_Coast_of_the_United_States "Gulf Coast of the United States") states beginning in [Texas](https://en.wikipedia.org/wiki/Texas "Texas") and ending in [Louisiana](https://en.wikipedia.org/wiki/Louisiana "Louisiana").
---
