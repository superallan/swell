---
layout: blog
title: Where We Wanna Be - Daði Freyr
date: 2020-06-28T18:15:34.285Z
media: https://youtu.be/gtr3zRDNoXU
context: '[Daði Freyr](https://en.wikipedia.org/wiki/Daði_Freyr) is an
  [Icelandic](https://en.wikipedia.org/wiki/Iceland "Iceland") musician living
  in [Berlin](https://en.wikipedia.org/wiki/Berlin "Berlin"), Germany. He was
  due to represent [Iceland in the Eurovision Song Contest
  2020](https://en.wikipedia.org/wiki/Iceland_in_the_Eurovision_Song_Contest_2020
  "Iceland in the Eurovision Song Contest 2020") with the song "[Think About
  Things](https://en.wikipedia.org/wiki/Think_About_Things "Think About
  Things")", before the [Eurovision Song Contest
  2020](https://en.wikipedia.org/wiki/Eurovision_Song_Contest_2020 "Eurovision
  Song Contest 2020") was cancelled.'
---
