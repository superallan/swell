---
layout: blog
title: How Beer Cans Are Made
date: 2020-06-27T14:41:59.751Z
media: https://youtu.be/V1fyfYa1MiE
context: "[Cask Brewing Systems](https://www.cask.com/) has been around since
  the early 1980s. It has seven stations in its canning rotation before it's
  shipped off to be sold."
---
