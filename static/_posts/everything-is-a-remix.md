---
layout: blog
title: Everything is a Remix
date: 2020-07-05T10:11:00.884Z
media: https://www.youtube.com/watch?v=nJPERZDfyWc
context: Everything is a Remix is produced by [Kirby
  Ferguson](https://www.everythingisaremix.info/about), a US-based filmmaker.
---
