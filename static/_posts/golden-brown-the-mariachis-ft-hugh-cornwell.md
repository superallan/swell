---
layout: blog
title: Golden Brown - The Mariachis ft Hugh Cornwell
date: 2020-07-11T10:07:25.325Z
media: https://www.youtube.com/watch?v=UQWL6XBg7Hc
context: >-
  ["**Golden Brown**"](https://en.wikipedia.org/wiki/Golden_Brown) is a song by
  the English rock band [the
  Stranglers](https://en.wikipedia.org/wiki/The_Stranglers "The Stranglers"). It
  was released as a 7" single, on
  [Liberty](https://en.wikipedia.org/wiki/Liberty_Records "Liberty Records"), in
  December 1981 in the United States and in January 1982 in the United Kingdom.
  It was the second single released from the band's sixth album *[La
  folie](https://en.wikipedia.org/wiki/La_folie_(album) "La folie (album)")*. It
  peaked at No. 2 in the [UK Singles
  Chart](https://en.wikipedia.org/wiki/UK_Singles_Chart "UK Singles Chart"), the
  band's highest ever placing in that
  chart.


  In January 2014, *[NME](https://en.wikipedia.org/wiki/NME "NME")* ranked the song as No. 488 on its list of the *500 Greatest Songs of All Time*. It has also been recorded by many other artists.


  There has been much controversy surrounding the lyrics. In his book *The Stranglers Song By Song* (2001), [Hugh Cornwell](https://en.wikipedia.org/wiki/Hugh_Cornwell "Hugh Cornwell") states "'Golden Brown' works on two levels. It's about [heroin](https://en.wikipedia.org/wiki/Heroin "Heroin") and also about a girl." Essentially the lyrics describe how "both provided me with pleasurable times."


  **[Mariachi](https://en.wikipedia.org/wiki/Mariachi)** is a genre of [Regional Mexican](https://en.wikipedia.org/wiki/Regional_Mexican "Regional Mexican") Spanish music that dates back to at least the 18th century, evolving over time in the countryside of various regions of western [Mexico](https://en.wikipedia.org/wiki/Mexico "Mexico"). The usual mariachi group today consists of as many as eight violins, two trumpets and at least one guitar, including a high-pitched [vihuela](https://en.wikipedia.org/wiki/Mexican_vihuela "Mexican vihuela") and an acoustic bass guitar called a [guitarrón](https://en.wikipedia.org/wiki/Guitarr%C3%B3n_mexicano "Guitarrón mexicano"), and all players taking turns singing lead and doing backup vocals.


  From the 19th to 20th century, migrations from rural areas into [Guadalajara](https://en.wikipedia.org/wiki/Guadalajara "Guadalajara"), along with the Mexican government's cultural promotion gradually re-labeled it as [*Son* style](https://en.wikipedia.org/wiki/Mexican_Son_music "Mexican Son music"), with its alternative name of “Mariachi” becoming used for the “urban” form. Modifications of the music include influences from other music such as [polkas](https://en.wikipedia.org/wiki/Polka "Polka") and [waltzes](https://en.wikipedia.org/wiki/Waltz "Waltz"), the addition of trumpets and the use of [charro outfits](https://en.wikipedia.org/wiki/Charro_outfit "Charro outfit") by mariachi musicians. The musical style began to take on national prominence in the first half of the 20th century, with its promotion at presidential inaugurations and on the radio in the 1920s. In 2011, [UNESCO](https://en.wikipedia.org/wiki/UNESCO "UNESCO") recognized Mariachi as an [Intangible Cultural Heritage](https://en.wikipedia.org/wiki/Intangible_cultural_heritage "Intangible cultural heritage"), joining six other entries on the Mexican list of that category.


  Song styles and instrumentals performed with Mariachi include [rancheras](https://en.wikipedia.org/wiki/Ranchera "Ranchera"), [corridos](https://en.wikipedia.org/wiki/Corrido "Corrido"), [cumbias](https://en.wikipedia.org/wiki/Cumbia "Cumbia"), [boleros](https://en.wikipedia.org/wiki/Bolero#Cuba "Bolero"), [ballads](https://en.wikipedia.org/wiki/Sentimental_ballad "Sentimental ballad"), [sones](https://en.wikipedia.org/wiki/Son_mexicano "Son mexicano"), [huapangos](https://en.wikipedia.org/wiki/Huapango "Huapango"), [jarabes](https://en.wikipedia.org/wiki/Jarabe "Jarabe"), [danzones](https://en.wikipedia.org/wiki/Danz%C3%B3n "Danzón"), [joropos](https://en.wikipedia.org/wiki/Joropo "Joropo"), [pasodobles](https://en.wikipedia.org/wiki/Pasodoble "Pasodoble"), [marches](https://en.wikipedia.org/wiki/March_(music) "March (music)"), [polkas](https://en.wikipedia.org/wiki/Polka "Polka"), [waltzes](https://en.wikipedia.org/wiki/Waltz "Waltz") and [chotís](https://en.wikipedia.org/wiki/Schottis "Schottis"). Most song lyrics are about machismo, love, betrayal, death, politics, revolutionary heroes and country life. One particularly famous song is “[La Cucaracha](https://en.wikipedia.org/wiki/La_Cucaracha "La Cucaracha")” (The Cockroach).
---
