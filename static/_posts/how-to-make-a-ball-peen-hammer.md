---
layout: blog
title: How To Make A Ball-peen Hammer
date: 2020-06-29T13:02:23.831Z
media: https://youtu.be/ZENofiIAvb4
context: >-
  Engaging young blacksmith makes a hammer. What's not to love.


  > My name is Alec Steele and I am a 21 year old blacksmith from Norfolk in the United Kingdom, now living in Montana in the USA! I upload a vlog from my day at the workshop almost every single day. Lots of sparks, lots of making, lots of fantastic-ness.
---
