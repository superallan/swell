---
layout: blog
title: Jolene - Dolly Parton - Bardcore cover by Hildegard von Blingin'
date: 2020-07-25T07:10:43.306Z
media: https://www.youtube.com/watch?v=ugqQlB5fpuc
context: >-
  > Jolene, Jolene, Jolene, Jolene \

  > I beg of thee, oh please take not my lord \

  > Jolene, Jolene, Jolene, Jolene \

  > I fear, from you, ‘twould take naught but a word \

  > Thou couldst have thy choice of men \

  > But I could never love again \

  > He is the only one for me \

  > Jolene


  Taken from ID's article titled *[Exploring Bardcore: YouTube’s obsession with medieval covers of Lady Gaga](https://i-d.vice.com/en_uk/article/v7gbqj/bardcore-youtube-music-trend-medieval-covers):*


  > [Hildegard von Blingin’](https://www.youtube.com/user/9freakydarling9)  is another key player in the Bardcore movement, whose videos have a complete commitment to old English -- from the song lyrics to the video description. ("Verily, I am flabbergasted by the response these days of late. Over one hundred thousand souls? Thank ye, from the bottom of my heart.”) While Cornelius Link’s videos are what inspired her to start creating covers, Hildegard’s own account has now overtaken his with more than 445k subscribers and over a million views on nearly every video posted.

  >

  > Hildegard chalks the popularity of the trend up to the universal need to escape “our own [ennui](https://i-d.vice.com/en_uk/article/v745nm/cultural-history-boredom-quarantaine)”. The YouTuber creates her covers in a small bedroom studio, using “a Celtic harp, a handful of recorders and whistles” and a pack of digital medieval instrument samples. “Many of us lost our jobs at the start of this pandemic and have a lot of time on our hands,” she says. “For one thing, that means more of us are making music and pursuing hobbies we wouldn't otherwise have time for, and more people are looking to YouTube and other social media for connection and distraction.”


  "**Jolene**" is a song written and performed by American [country music](https://en.wikipedia.org/wiki/Country_music "Country music") artist [Dolly Parton](https://en.wikipedia.org/wiki/Dolly_Parton "Dolly Parton"). It was released on October 15, 1973 as the first single and title track from her album [of the same name](https://en.wikipedia.org/wiki/Jolene_(album) "Jolene (album)"), produced by [Bob Ferguson](https://en.wikipedia.org/wiki/Bob_Ferguson_(musician) "Bob Ferguson (musician)").


  According to Parton, the song was inspired by a red-headed bank clerk who flirted with her husband Carl Dean at his local bank branch around the time they were newly married. In an interview, she also revealed that Jolene's name and appearance are based on that of a young fan who came on stage for her autograph.[](https://en.wikipedia.org/wiki/Jolene_(song)?wprov=srpw1_0


  The song became Parton's second solo number-one single on the country charts after being released as a single in October 1973 (prior to the album's release). It reached the top position in February 1974; it was also a moderate pop hit for her and a minor adult contemporary chart entry.
---
