---
layout: blog
title: Antoni Porowski Gets a Drag Makeover from Miz Cracker
date: 2020-06-30T13:22:58.168Z
media: https://youtu.be/tUw1Q2NP9As
context: >-
  [Antoni Porowski](https://en.wikipedia.org/wiki/Antoni_Porowski) is a Canadian
  television personality, [actor](https://en.wikipedia.org/wiki/Actor "Actor"),
  chef, and [model](https://en.wikipedia.org/wiki/Model_(person) "Model
  (person)"). He is known for his role as the food and wine
  [expert](https://en.wikipedia.org/wiki/Expert "Expert") on the
  [Emmy](https://en.wikipedia.org/wiki/Emmy "Emmy") award-winning
  [Netflix](https://en.wikipedia.org/wiki/Netflix "Netflix") series *[Queer
  Eye](https://en.wikipedia.org/wiki/Queer_Eye_(2018_TV_series) "Queer Eye (2018
  TV series)")*.


  [Miz Cracker](https://en.wikipedia.org/wiki/Miz_Cracker) is the stage name of Maxwell Heller, an American [drag queen](https://en.wikipedia.org/wiki/Drag_queen "Drag queen") and television personality. She is best known for placing fifth on the [tenth season](https://en.wikipedia.org/wiki/RuPaul%27s_Drag_Race_(season_10) "RuPaul's Drag Race (season 10)") of the reality competition television series *[RuPaul's Drag Race](https://en.wikipedia.org/wiki/RuPaul%27s_Drag_Race "RuPaul's Drag Race")*.
---
