---
layout: blog
title: Lemon Jelly - Only Time
date: 2020-06-29T08:01:57.252Z
media: https://youtu.be/5cf_JFo-TYI
context: >-
  [Lemon Jelly](https://en.wikipedia.org/wiki/Lemon_Jelly) is a
  [British](https://en.wikipedia.org/wiki/Music_of_the_United_Kingdom "Music of
  the United Kingdom") [electronic
  music](https://en.wikipedia.org/wiki/Electronic_music "Electronic music") duo
  from [London](https://en.wikipedia.org/wiki/London "London") that formed in
  1998 and went on hiatus starting in 2008. Since its inception, the band
  members have always been [Fred
  Deakin](https://en.wikipedia.org/wiki/Fred_Deakin "Fred Deakin") and [Nick
  Franglen](https://en.wikipedia.org/wiki/Nick_Franglen "Nick Franglen"). Lemon
  Jelly has been nominated for awards like the [Mercury Music
  Prize](https://en.wikipedia.org/wiki/Mercury_Music_Prize "Mercury Music
  Prize") and [BRIT Awards](https://en.wikipedia.org/wiki/BRIT_Award "BRIT
  Award"). The bright colorful artwork featured in the albums and music videos,
  and the Lemon Jelly typeface, became part of the "brand".


  Deakin and Franglen briefly met in [north London](https://en.wikipedia.org/wiki/North_London "North London") as teenagers and became friends before going their separate ways: Deakin became a [DJ](https://en.wikipedia.org/wiki/DJ "DJ") and co-founded [Airside studios](https://en.wikipedia.org/wiki/Airside_(company) "Airside (company)"); Franglen became a studio programmer. The two became reacquainted in 1998 and created the group Lemon Jelly.


  Lemon Jelly released three critically acclaimed EPs (1998, 1999, and 2000), securing them a [record deal](https://en.wikipedia.org/wiki/Record_deal "Record deal") with [XL Recordings](https://en.wikipedia.org/wiki/XL_Recordings "XL Recordings") in 2000. The band subsequently released three [full-length albums](https://en.wikipedia.org/wiki/Album "Album") before going on hiatus in 2008.
---
