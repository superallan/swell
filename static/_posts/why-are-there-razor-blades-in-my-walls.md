---
layout: blog
title: Why Are There Razor Blades In My Walls?
date: 2020-06-27T18:35:08.631Z
media: https://youtu.be/79GlUDVJXaA
context: >-
  Many people have opened up the walls of old houses only to have an avalanche
  of rusty old blades fall out. Fifty years ago, nearly every bathroom had a
  medicine cabinet. At the back of the medicine cabinet was a tiny slot for
  disposing of use shaving razors. But the slot was actually nothing more than a
  hole in the wall. So, once you put something through that slot, there was no
  way to get it out. 


  Over the years, dirty used razor blades would pile up inside the wall. When the medicine cabinets were put in, the thinking must have been that it would take countless years to fill a wall with garbage razors. Out of sight of sight and out of mind, it was a problem for someone else to deal with. There’s something amusing about the fact that only a few decades later, we’re finding these rusty old razors and some of us are completely confused by it.
---
