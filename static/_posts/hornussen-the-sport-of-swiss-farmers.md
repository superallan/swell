---
layout: blog
title: Hornussen - the sport of Swiss farmers
date: 2020-06-27T12:19:03.669Z
media: https://youtu.be/GdIHOV6VGNU
context: >-
  [Hornussen](https://en.wikipedia.org/wiki/Hornussen) is an indigenous
  [Swiss](https://en.wikipedia.org/wiki/Switzerland "Switzerland") sport. The
  sport gets its name from the [puck](https://en.wikipedia.org/wiki/Hockey_puck
  "Hockey puck") which is known as a "Hornuss"
  ([hornet](https://en.wikipedia.org/wiki/Hornet "Hornet")) or "Nouss". When
  hit, it can whizz through the air at up to 300 km/h (186.4 mph) and create a
  buzzing
  sound.


  Together with [Schwingen](https://en.wikipedia.org/wiki/Schwingen "Schwingen") and [Steinstossen](https://en.wikipedia.org/wiki/Steinstossen "Steinstossen"), Hornussen is seen as a Swiss national sport. Outside of Switzerland, there are only a few teams.
---
