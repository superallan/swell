---
layout: blog
title: The Communal Art of Footracing
date: 2020-06-28T18:38:15.419Z
media: https://youtu.be/NxmFbGiJyiI
context: Every weekend, tens of thousands of people, line up at a start line to
  run a footrace. Why? Running and competing is such a historic and intrinsic
  part of our culture that we don’t often question it. “The Communal Art of
  Footracing” explores the meaning behind our competitive drive, how this
  translates to Ultra-Trail Running and why it is so important
---
