---
layout: blog
title: David Bowie - Changes (Glastonbury 2000)
date: 2020-06-29T07:56:10.987Z
media: https://youtu.be/dwdQRrMUbGs
context: >-
  From the magnificent headline set at the [Glastonbury
  Festival](https://en.wikipedia.org/wiki/Glastonbury_Festival), 25 June 2000;
  much of which was not broadcast live at the time, apparently at
  [Bowie’s](https://en.wikipedia.org/wiki/David_Bowie) own insistence.


  David Bowie was an English singer-songwriter and actor. He was a leading figure in the music industry and is considered one of the most influential musicians of the 20th century. He was acclaimed by critics and musicians, particularly for his innovative work during the 1970s. His career was marked by reinvention and visual presentation, with his music and stagecraft having a significant impact on [popular music](https://en.wikipedia.org/wiki/Popular_music "Popular music"). During his lifetime, his record sales, estimated at over 100 million records worldwide, made him one of the [world's best-selling music artists](https://en.wikipedia.org/wiki/List_of_best-selling_music_artists "List of best-selling music artists"). In the UK, he was awarded ten [platinum](https://en.wikipedia.org/wiki/Music_recording_certification "Music recording certification") album certifications, eleven gold and eight silver, and released [eleven number-one albums](https://en.wikipedia.org/wiki/List_of_artists_by_number_of_UK_Albums_Chart_number_ones "List of artists by number of UK Albums Chart number ones"). In the US, he received five platinum and nine gold certifications. He was inducted into the [Rock and Roll Hall of Fame](https://en.wikipedia.org/wiki/Rock_and_Roll_Hall_of_Fame "Rock and Roll Hall of Fame") in 1996. *[Rolling Stone](https://en.wikipedia.org/wiki/Rolling_Stone "Rolling Stone")*placed him among its list of the [100 Greatest Artists of All Time](https://en.wikipedia.org/wiki/Rolling_Stone%27s_100_Greatest_Artists_of_All_Time "Rolling Stone's 100 Greatest Artists of All Time") and following his death in 2016, Bowie was dubbed "The Greatest Rock Star Ever" by the magazine.


  Glastonbury Festival  is a five-day festival of contemporary performing arts that takes place in [Pilton](https://en.wikipedia.org/wiki/Pilton,_Somerset "Pilton, Somerset"), [Somerset](https://en.wikipedia.org/wiki/Somerset "Somerset"), in England. In addition to contemporary music, the festival hosts dance, comedy, theatre, circus, cabaret, and other arts. Leading pop and rock artists have headlined, alongside thousands of others appearing on smaller stages and performance areas. Films and albums recorded at Glastonbury have been released, and the festival receives extensive television and newspaper coverage. Glastonbury is now attended by around 200,000 people, requiring extensive infrastructure in terms of security, transport, water, and electricity supply. The majority of staff are volunteers, helping the festival to raise millions of pounds for charity organisations.
---
