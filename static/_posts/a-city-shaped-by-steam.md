---
layout: blog
title: A City Shaped by Steam
date: 2020-07-08T08:37:59.246Z
media: https://www.youtube.com/watch?v=mAB1gesb8cQ
context: >-
  First developed in the 1880s, [New York
  City](https://en.wikipedia.org/wiki/New_York_City)’s steam system is the
  largest in the world. No other urban steam system comes close.


  Today, 105 miles of steam pipe run beneath the streets of the city, delivering steam to 2,000 buildings for heating and cooling. Steam also sterilizes hospital equipment, presses clothes, and cleans restaurant dishes and cutlery.


  This episode of “Living City,” a video series about New York’s infrastructure, looks at the history of the city’s steam system and explores how a technology that eliminated chimneys from the skyline in the early 20th century is helping reduce carbon emissions and provide a cleaner source of energy for New York in the 21st.


  The film tours the East 14th Street [Consolidated Edison](https://en.wikipedia.org/wiki/Consolidated_Edison) cogeneration plant, where 55 percent of the city’s steam is produced, and looks at the underbelly of some of New York’s most recent buildings to see how steam is incorporated into modern urban planning and design.


  At New York University, a steam system generates electricity and controls the climate in campus buildings. According to the school, the steam system lowers operating costs and improves the environment.
---
