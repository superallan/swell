---
layout: blog
title: Jordan Peterson on Astrology
date: 2020-07-01T08:24:05.559Z
media: https://www.youtube.com/watch?v=xqSRy2cm7uQ
context: >-
  A few words from [Jordan
  Peterson](https://en.wikipedia.org/wiki/Jordan_Peterson) on
  [Astrology](https://en.wikipedia.org/wiki/Astrology). Although he apparently
  does not agree entirely with astrology, Jordan Peterson however acknowledges
  the importance of astrology as a semantic system of knowledge that served as a
  forerunner to modern science and philosophy.


  [Carl Jung](https://en.wikipedia.org/wiki/Carl_Jung) found that the ancients devised astrology in an effort to categorize human beings, much in the way we now use the [Myers-Briggs Type Indicator](https://en.wikipedia.org/wiki/Myers–Briggs_Type_Indicator) that was derived from Jung's theory of personality types. He found creating [astrological natal charts](https://en.wikipedia.org/wiki/Natal_astrology) for his patients gave him uncanny insight into his patients' minds. Jung thought this was due to an ongoing cyclical projection of mass [psychology](https://en.wikipedia.org/wiki/Psychology) into the night sky.
---
