---
layout: blog
title: Production of Gabion Structures
date: 2020-06-28T09:32:17.935Z
media: https://www.youtube.com/watch?v=UUhPMtsQwdo
context: >-
  A [gabion](https://en.wikipedia.org/wiki/Gabion) (from
  [Italian](https://en.wikipedia.org/wiki/Italian_language "Italian language")
  *gabbione* meaning "big cage"; from Italian *gabbia* and Latin *cavea* meaning
  "cage") is a [cage](https://en.wikipedia.org/wiki/Cage_(enclosure) "Cage
  (enclosure)"), [cylinder](https://en.wikipedia.org/wiki/Cylinder_(geometry)
  "Cylinder (geometry)") or box filled with rocks, concrete, or sometimes sand
  and soil for use in civil engineering, road building, military applications
  and [landscaping](https://en.wikipedia.org/wiki/Landscaping "Landscaping").


  For erosion control, caged [riprap](https://en.wikipedia.org/wiki/Riprap "Riprap") is used. For dams or in foundation construction, cylindrical metal structures are used. In a military context, earth- or sand-filled gabions are used to protect [sappers](https://en.wikipedia.org/wiki/Sapper "Sapper"), infantry, and [artillerymen](https://en.wikipedia.org/wiki/Artillery "Artillery") from enemy fire.
---
