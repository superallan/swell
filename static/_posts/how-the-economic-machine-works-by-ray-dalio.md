---
layout: blog
title: How The Economic Machine Works by Ray Dalio
date: 2020-06-28T09:39:26.441Z
media: https://youtu.be/PHe0bXAIuk0
context: >-
  Created by Ray Dalio this simple but not simplistic and easy to follow 30
  minute, animated video answers the question, "How does the economy really
  work?" 


  [Ray Dalio](https://en.wikipedia.org/wiki/Ray_Dalio) (born August 8, 1949) is an American billionaire [hedge fund](https://simple.wikipedia.org/wiki/Hedge_fund "Hedge fund") manager, author, and [philanthropist](https://simple.wikipedia.org/wiki/Philanthropy "Philanthropy"). He is the co-chief investment officer of [Bridgewater Associate](https://en.wikipedia.org/wiki/Bridgewater_Associates) since the company was founded in 1985.


  Based on Dalio's practical template for understanding the economy, which he developed over the course of his career, the video breaks down economic concepts like credit, deficits and interest rates, allowing viewers to learn the basic driving forces behind the economy, how economic policies work and why economic cycles occur.
---
