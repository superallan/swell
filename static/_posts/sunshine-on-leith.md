---
layout: blog
title: Sunshine On Leith
date: 2020-06-27T15:23:39.335Z
media: https://youtu.be/UEeXJH3EXMY
context: >-
  The 2016 Scottish Cup Final was the 131st final of the [Scottish
  Cup](https://en.wikipedia.org/wiki/Scottish_Cup "Scottish Cup") and the final
  of the [2015–16 Scottish
  Cup](https://en.wikipedia.org/wiki/2015%E2%80%9316_Scottish_Cup "2015–16
  Scottish Cup"), the most prestigious knockout
  [football](https://en.wikipedia.org/wiki/Association_football "Association
  football") competition in Scotland.


  Hibernian ended a run of 114 years from last winning the competition, beating [Rangers](https://en.wikipedia.org/wiki/Rangers_F.C.) 3–2 with a stoppage time goal from [club captain](https://en.wikipedia.org/wiki/Captain_(association_football) "Captain (association football)") [David Gray](https://en.wikipedia.org/wiki/David_Gray_(footballer,_born_1988) "David Gray (footballer, born 1988)").


  ["Sunshine on Leith"](https://en.wikipedia.org/wiki/Sunshine_on_Leith_(song)) is a [ballad](https://en.wikipedia.org/wiki/Ballad "Ballad") by Scottish [folk rock](https://en.wikipedia.org/wiki/Folk_rock "Folk rock") duo [The Proclaimers](https://en.wikipedia.org/wiki/The_Proclaimers "The Proclaimers"). Released in 1988, it is the title-track and second single from their album *[Sunshine on Leith](https://en.wikipedia.org/wiki/Sunshine_on_Leith_(album) "Sunshine on Leith (album)")*. In June 2018, "Sunshine on Leith" was voted the UK's favourite [football anthem](https://en.wikipedia.org/wiki/Football_chant "Football chant") as part of the "Football Anthems World Cup" by Steve Lemacq on [BBC Radio 6](https://en.wikipedia.org/wiki/BBC_Radio_6 "BBC Radio 6").
---
