---
layout: blog
title: Top 5 "Classic" Bourbons
date: 2020-06-29T13:07:56.997Z
media: https://youtu.be/m3lfrhRcegU
context: "> We asked the Whiskey Tribe to vote up the epitome of BOURBON, and
  the Magnificent Bastards answered with 5 exceptional examples of CLASSIC
  BOURBON. Every Magnificent Bastard knows that taste can be extremely
  subjective - especially when it comes to whisk(e)y. (see Rule #1: The \"best\"
  whiskey is the whiskey you like to drink - the way you like to drink it.) But
  is there a consensus for what common flavors define bourbon as a category?
  Here's your results for the most CLASSIC / QUINTESSENTIAL / REPRESENTATIVE /
  PROTOTYPICAL / CONSUMMATE bourbons available today."
---
