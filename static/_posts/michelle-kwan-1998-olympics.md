---
layout: blog
title: Michelle Kwan - 1998 Olympics
date: 2020-06-29T08:09:51.990Z
media: https://youtu.be/OHfgjszz_Tk
context: >-
  [Michelle
  Kwan](https://en.wikipedia.org/wiki/Michelle_Kwan)
  (born July 7, 1980) is a retired American [figure
  skater](https://en.wikipedia.org/wiki/Figure_skating "Figure skating"). She is
  a two-time [Olympic
  medalist](https://en.wikipedia.org/wiki/List_of_Olympic_medalists_in_figure_skating
  "List of Olympic medalists in figure skating") (silver in 1998, bronze in
  2002), a five-time [World
  champion](https://en.wikipedia.org/wiki/World_Figure_Skating_Championships
  "World Figure Skating Championships") (1996, 1998, 2000, 2001, 2003) and a
  nine-time [U.S.
  champion](https://en.wikipedia.org/wiki/United_States_Figure_Skating_Championships
  "United States Figure Skating Championships") (1996, 1998–2005). She is tied
  with [Maribel Vinson](https://en.wikipedia.org/wiki/Maribel_Vinson "Maribel
  Vinson") for the all-time National Championship record.


  She competed at the senior level for over a decade and is the most decorated figure skater in U.S. history. Known for her consistency and expressive artistry on ice, she is widely considered one of the greatest figure skaters of all time.


  For well over a decade, Kwan maintained her status not only as America's most popular figure skater but as one of America's most popular female athletes. During her reign Kwan landed numerous major endorsement deals, starred in multiple TV specials and was the subject of extensive media coverage
---
