---
layout: blog
title: No More Jockeys
date: 2020-06-28T19:58:32.938Z
media: https://youtu.be/4xIulxG7_k8
context: >-
  No More Jockeys is a game created by [Alex
  Horne](https://en.wikipedia.org/wiki/Alex_Horne), [Tim
  Key](https://en.wikipedia.org/wiki/Tim_Key) and [Mark
  Watson](https://en.wikipedia.org/wiki/Mark_Watson). 


  On each turn, players name a person plus a category they fall under. That person and category are then eliminated, and subsequent people must not fall under that category. As more categories are added it gets harder, and eventually impossible, to name anyone new.
---
