---
layout: blog
title: GBDuro 2019
date: 2020-07-01T14:45:20.807Z
media: https://www.youtube.com/watch?v=e74xncSCoqw
context: '[Rapha](https://en.wikipedia.org/wiki/Rapha_(sportswear)) and [EF
  Education First](https://en.wikipedia.org/wiki/EF_Pro_Cycling) (a World Tour
  level pro cycling team) have been making some really interesting and
  inspirational films about the exploits of some of their riders in "alternative
  calendar" cycling events - this is a particularly choice example of [Lachlan
  Morton](https://en.wikipedia.org/wiki/Lachlan_Morton) riding the
  [GBDuro](https://www.theracingcollective.com/gbduro.html) last year; an
  unsupported multi-day race across the length of the UK.'
---
