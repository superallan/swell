---
layout: blog
title: TO-ST1-T Bread Oven
date: 2020-06-27T14:43:57.914Z
media: https://youtu.be/IxAbz9mfaj0
context: “We wanted to focus on the single slice, and treat it with respect,”
  said Akihiro Iwahara, who is in charge of technical development at [Mitsubishi
  Electric](http://www.mitsubishielectric.com/)’s home-appliances division. “Our
  technology and know-how from rice cookers helped us come up with a way to trap
  and seal moisture.” Hence Mitsubishi designed the oven ensuring only one slice
  of bread could be cooked in one go.
---
