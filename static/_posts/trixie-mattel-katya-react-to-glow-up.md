---
layout: blog
title: Trixie Mattel & Katya React to Glow Up
date: 2020-06-27T15:08:54.733Z
media: https://youtu.be/NQhe1TDn7VI
context: >-
  Trixie Mattel is the drag persona of Brian Michael Firkus, an American
  [drag queen](https://en.wikipedia.org/wiki/Drag_queen "Drag queen"),
  singer-songwriter, comedian, and [television
  personality](https://en.wikipedia.org/wiki/Television_personality "Television
  personality").


  Yekaterina Petrovna Zamolodchikova ([Russian](https://en.wikipedia.org/wiki/Russian_language "Russian language"): Екатерина Петро́вна Замоло́дчикова), often known [mononymously](https://en.wikipedia.org/wiki/Mononymous_person "Mononymous person") as Katya ([Russian](https://en.wikipedia.org/wiki/Russian_language "Russian language"): Катя), is the [drag persona](https://en.wikipedia.org/wiki/Drag_queen "Drag queen") of Brian Joseph McCook, an American [drag queen](https://en.wikipedia.org/wiki/Drag_queen "Drag queen"), actor and comedian.


  Both are most well known for appearing as on [RuPaul's Drag Race](https://en.wikipedia.org/wiki/RuPaul%27s_Drag_Race).


  [Glow Up](https://en.wikipedia.org/wiki/Glow_Up:_Britain%27s_Next_Make-Up_Star) is a [British](https://en.wikipedia.org/wiki/Television_in_the_United_Kingdom "Television in the United Kingdom") [reality television](https://en.wikipedia.org/wiki/Reality_television "Reality television") competition devised to find new [makeup artists](https://en.wikipedia.org/wiki/Makeup_artists "Makeup artists"). Hosted by [Stacey Dooley](https://en.wikipedia.org/wiki/Stacey_Dooley "Stacey Dooley"), the first episode premiered on [BBC Three](https://en.wikipedia.org/wiki/BBC_Three "BBC Three") on 6 March 2019. The contestants take part in weekly challenges to progess through the competition, which are judged by industry professionals Dominic Skinner and Val Garland, as well as weekly guest stars.
---
