---
layout: blog
title: Johnny White Really-Really Used to be a Wedding DJ
date: 2020-08-18T12:48:00.000Z
media: https://youtu.be/V0vMGfYQldU
context: >-
  > Exuding a quiet indie-comedy vibe, [Johnny White
  Really-Really](https://www.chortle.co.uk/review/2019/08/18/44049/johnny_white_really-really:_unending_torment)
  must be one of the most understated stand-ups on the circuit. He just talks,
  with almost no animation, about the mundanity of his daily life. When he does
  need to make a gesture, it’s halting and unconfident. He’s even apologetic
  about playing a sound cue.

  >

  > But out of the modest and the unexceptional, some highly creative comedy emerges, as minor incidents spiral into surreal stories or complex, real reminiscences. Being hit by an errant apple thrown by some kids, or having an awkward conversation with a supermarket cashier – these are the things that prey on his mind for weeks.

  >

  > It feels that he has created fantasy worlds where he’s hanging out with Ben Affleck or attending the wedding of Tom (from Tom & Jerry) because his shyness makes it difficult to fit into reality. He references his loneliness, lending a poignancy to his inability to muster a mate to have a drink with, and describing himself as a ‘high-functioning alcoholic’.

  >

  > Perhaps his peculiarities stem from childhood, when he vividly remembers being excluded a 12th birthday party. His reaction to it sounds like he put himself through psychological torture, and the memory still burns bright in his cortex. To this day, he continues to see humanity as a party he’s not been invited to.

  >

  > He laughs at his own ideas sometimes, in an entirely natural way that’s all part of his utter lack of affectation. When he imagines a possible sequel to the Wicker Man, it seems purely for his own enjoyment; if we get on board, so much the better. And we do, for he is delightful in his vulnerabilities.

  >

  > Another part of his appeal is his almost poetic use of language. For example, he describes Affleck’s terrible choice of music as ‘the type of music that would be in a computer game about snowboarding… but not even when you are on the piste.’

  >

  > Such lyricism is perfectly suited to White Really-Really’s delicate state on the intersection of funny and sad. Few comics could drop in a Phillip Larkin poem without skipping a beat, but it segues naturally from the comedian’s dwelling on the meaning of life. Not in general, just his, but the observations are at once unique and identifiable.

  >

  >
---
