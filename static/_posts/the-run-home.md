---
layout: blog
title: The Run Home
date: 2020-07-01T14:39:14.924Z
media: https://www.youtube.com/watch?v=6klZXokN_gw
context: >-
  [Hill runner](https://en.wikipedia.org/wiki/Fell_running) Louis MacMillan, 17,
  takes the scenic route home from school in
  [Argyll](https://en.wikipedia.org/wiki/Argyll), in this superb video shot by
  his pal Alfie Smith.


  **Fell running**, also sometimes known as **hill running**, is the sport of [running](https://en.wikipedia.org/wiki/Running "Running") and racing, off-road, over upland country where the gradient climbed is a significant component of the difficulty. The name arises from the origins of the English sport on the [fells](https://en.wikipedia.org/wiki/Fell "Fell") of northern Britain, especially those in the [Lake District](https://en.wikipedia.org/wiki/Lake_District "Lake District"). It has elements of trail running, cross country and mountain running, but is also distinct from those disciplines.


  Fell races are organised on the premise that contenders possess mountain [navigation](https://en.wikipedia.org/wiki/Navigation "Navigation") skills and carry adequate survival equipment as prescribed by the organiser.


  Fell running has common characteristics with [cross-country running](https://en.wikipedia.org/wiki/Cross-country_running "Cross-country running"), but is distinguished by steeper gradients and upland country. It is sometimes considered as a form of [mountain running](https://en.wikipedia.org/wiki/Mountain_running "Mountain running"), but without the smoother trails and predetermined routes often associated with mountain running.
---
