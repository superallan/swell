---
layout: blog
title: Guacamole - Will It Soft Serve?
date: 2020-07-05T16:03:16.598Z
media: https://youtu.be/i0vg2f82-Ok
context: "[Avocados](https://en.wikipedia.org/wiki/Avocado) are creamy anyway,
  so surely [guacamole](https://en.wikipedia.org/wiki/Guacamole) will turn into
  [ice cream](https://en.wikipedia.org/wiki/Ice_cream)‽"
---
