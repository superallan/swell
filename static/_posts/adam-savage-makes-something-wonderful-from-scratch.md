---
layout: blog
title: Adam Savage Makes Something Wonderful from Scratch
date: 2020-06-27T14:51:45.612Z
media: https://youtu.be/-tUHJnl8qPM
context: Watch [Adam Savage](https://en.wikipedia.org/wiki/Adam_Savage) build,
  from start to finish, a stylized box to carry and display his [Blade
  Runner](https://en.wikipedia.org/wiki/Blade_Runner) Blaster prop replica. The
  entire project took less than one day to complete, and Adam narrates this
  video with commentary about his design and construction methods. Find photos
  of this build and the completed box
  [here](https://www.tested.com/art/makers/453522-behind-scenes-adam-savages-one-day-box-build/).
---
