---
layout: blog
title: Maradona Warming Up
date: 2020-06-23T13:23:29.024Z
media: https://www.youtube.com/watch?v=Cdf6D19Etmc
context: >-
  [Diego Maradona](https://en.wikipedia.org/wiki/Diego_Maradona), is widely
  regarded as one of the greatest football players of all time.


  He was one of the two joint winners of the FIFA Player of the 20th Century award. Maradona's vision, passing, ball control and dribbling skills were combined with his small stature (1.65 m or 5 ft 5 in), which gave him a low center of gravity allowing him to maneuver better than most other football players; he would often dribble past multiple opposing players on a run. His presence and leadership on the field had a great effect on his team's general performance, while he would often be singled out by the opposition. In addition to his creative abilities, he also possessed an eye for goal and was known to be a free kick specialist. A precocious talent, Maradona was given the nickname "*El Pibe de Oro*" ("The Golden Boy"), a name that stuck with him throughout his career.


  The American newspaper *[The Houston Chronicle](https://en.wikipedia.org/wiki/The_Houston_Chronicle "The Houston Chronicle")* wrote about Maradona:


  > To understand the gargantuan shadow Maradona casts over his football-mad homeland, one has to conjure up the athleticism of Michael Jordan, the power of Babe Ruth – and the human fallibility of Mike Tyson. Lump them together in a single barrel-chested man with shaggy black hair and you have *El Diego*, idol to the millions who call him D10S, a mashup of his playing number and the Spanish word for God.
---
