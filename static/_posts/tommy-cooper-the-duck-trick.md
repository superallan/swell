---
layout: blog
title: Tommy Cooper - The Duck Trick
date: 2020-06-27T10:49:29.767Z
media: https://youtu.be/5yNzkMqBToc
context: "[Tommy Cooper](https://en.wikipedia.org/wiki/Tommy_Cooper) (19 March
  1921 -- 15 April 1984) was an English-Welsh prop comedian and magician. He was
  known for making an art of getting magic tricks wrong, although he was
  actually an accomplished magician and entertainer."
---
