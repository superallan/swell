---
layout: blog
title: "SUSHI Recipe Relay "
date: 2020-06-29T12:11:18.430Z
media: https://youtu.be/2JJu7RZYQm4
context: >-
  [SORTEDfood](https://en.wikipedia.org/wiki/Sorted_Food) is a British YouTube
  cooking channel and food website created by old school friends from
  Hertfordshire - Benjamin 'Ben' Ebbrell, Michael 'Mike' Huttlestone, Jamie
  Spafford and Barry
  Taylor. 


  Around 2014 James Currie, who met Ebbrell in culinary school, joined the team first as a developmental chef and then as an on camera personality. The Youtube channel was created on March 10, 2010 and has grown to attract an audience so large that in January 2019 Google described SORTEDfood as "one of the world's largest food and cooking communities".


  They have also created the Sorted Club (stylized as sorted club), which is a subscription based collection of apps to "learn, explore and change your routine for the better".
---
