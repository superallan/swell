---
layout: blog
title: Noam Chomsky - The 5 Filters of the Mass Media Machine
date: 2020-06-28T09:36:32.646Z
media: https://youtu.be/34LGPIXvU5M
context: >-
  According to American linguist and political activist, [Noam
  Chomsky](https://en.wikipedia.org/wiki/Noam_Chomsky), media operate through 5
  filters: ownership, advertising, the media elite, flak and the common enemy.


  Avram Noam Chomsky (born December 7, 1928) is an [American](https://simple.wikipedia.org/wiki/Americans "Americans") [linguist](https://simple.wikipedia.org/wiki/Linguistics "Linguistics"), [philosopher](https://simple.wikipedia.org/wiki/Philosopher "Philosopher"), [political activist](https://simple.wikipedia.org/wiki/Political_activist "Political activist"), [author](https://simple.wikipedia.org/wiki/Author "Author"), and [lecturer](https://simple.wikipedia.org/wiki/Lecturer "Lecturer"). He is an Institute Professor and professor [emeritus](https://simple.wikipedia.org/wiki/Emeritus "Emeritus") of linguistics at the [Massachusetts Institute of Technology](https://simple.wikipedia.org/wiki/Massachusetts_Institute_of_Technology "Massachusetts Institute of Technology").
---
