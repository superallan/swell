---
layout: blog
title: History of Distortion
date: 2020-07-01T14:42:15.000Z
media: https://www.youtube.com/watch?v=XTGmfsKHcXo
context: "[Distortion](https://en.wikipedia.org/wiki/Distortion) is completely
  ubiquitous in [guitar music](https://en.wikipedia.org/wiki/Guitar) but has a
  fascinating past - this video goes over the slightly random and definitely
  quite unintentional origins of distortion, and its increasing relevance over
  the years."
---
