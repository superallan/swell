---
layout: blog
title: The Iterated Prisoner's Dilemma and The Evolution of Cooperation
date: 2020-07-22T09:16:34.059Z
media: https://www.youtube.com/watch?v=BOvAbjfJ0x0
context: >-
  The iterated [prisoner's
  dilemma](https://en.wikipedia.org/wiki/Prisoner%27s_dilemma) is just like the
  regular game except you play it multiple times with an opponent and add up the
  scores. But it can change the strategy and has more real world applications as
  it resembles a relationship.


  The **prisoner's dilemma** is a standard example of a game analyzed in [game theory](https://en.wikipedia.org/wiki/Game_theory "Game theory") that shows why two completely [rational](https://en.wikipedia.org/wiki/Rationality#Economics "Rationality") individuals might not cooperate, even if it appears that it is in their best interests to do so. It was originally framed by [Merrill Flood](https://en.wikipedia.org/wiki/Merrill_Flood "Merrill Flood") and [Melvin Dresher](https://en.wikipedia.org/wiki/Melvin_Dresher "Melvin Dresher") while working at [RAND](https://en.wikipedia.org/wiki/RAND_Corporation "RAND Corporation") in 1950. [Albert W. Tucker](https://en.wikipedia.org/wiki/Albert_W._Tucker "Albert W. Tucker") formalized the game with prison sentence rewards and named it "prisoner's dilemma", presenting it as follows:


  > Two members of a criminal gang are arrested and imprisoned. Each prisoner is in solitary confinement with no means of communicating with the other. The prosecutors lack sufficient evidence to convict the pair on the principal charge, but they have enough to convict both on a lesser charge. Simultaneously, the prosecutors offer each prisoner a bargain. Each prisoner is given the opportunity either to betray the other by testifying that the other committed the crime, or to cooperate with the other by remaining silent. The possible outcomes are:

  >

  > * If A and B each betray the other, each of them serves two years in prison

  > * If A betrays B but B remains silent, A will be set free and B will serve three years in prison

  > * If A remains silent but B betrays A, A will serve three years in prison and B will be set free

  > * If A and B both remain silent, both of them will serve only one year in prison (on the lesser charge).
---
