---
layout: blog
title: Naval architect adapts boatbuilding techniques to create an otherworldly
  lunar lander
date: 2020-08-20T15:12:22.833Z
media: https://youtu.be/PCoymiQ13s8
context: >-
  > HOUSTON, WE HAVE a project. Technically, it is not a starship, but it
  certainly has proved quite the enterprise.

  >

  > Our DIY commander, Kurt Hughes, has systematically designed and built his very own livable, lovable lunar lander, securely perched on an acre of peaceful Central Washington terrain. It is out-of-this-world spectacular.

  >

  > Hughes launched this inspired mission about 10 years ago: His youngest daughter, Kiku, attended space camp at The Museum of Flight, so Hughes printed her a lunar-module graphic, and then actual Apollo 13 astronaut Fred Haise signed it. (The graphic, now framed, hangs in a spot of honor just inside the lander’s portal.)

  >

  > “I never have seen anyone living in a lunar lander,” Hughes says. “They’re mostly uninhabitable. But Haise said to my daughter: ‘The one we took was pretty comfortable.’ ”


  [Full article available at Seattle Times](https://www.seattletimes.com/pacific-nw-magazine/a-naval-architect-adapts-boatbuilding-techniques-to-create-an-otherworldly-lunar-lander/)
---
