---
layout: blog
title: The hippest internet cafe of 1995
date: 2020-08-26T11:30:17.271Z
media: https://youtu.be/iWssRVJgPqc
context: "[Vox](https://en.wikipedia.org/wiki/Vox_Media)'s Phil Edwards spoke to
  one of the founders of @ Cafe, [an internet
  cafe](https://en.wikipedia.org/wiki/Internet_café) that launched just as the
  internet was coming into the public eye."
---
