---
layout: blog
title: Bugsy Malone - So You Wanna Be A Boxer
date: 2020-08-01T08:00:13.664Z
media: https://youtu.be/mVZmW59AAgw
context: >-
  ***[Bugsy Malone](https://en.wikipedia.org/wiki/Bugsy_Malone)*** is a 1976
  [gangster](https://en.wikipedia.org/wiki/Gangster_film "Gangster film")
  [musical](https://en.wikipedia.org/wiki/Musical_film "Musical film") [comedy
  film](https://en.wikipedia.org/wiki/Comedy_film "Comedy film") written and
  directed by [Alan Parker](https://en.wikipedia.org/wiki/Alan_Parker "Alan
  Parker"). The film was Parker's feature film [directorial
  debut](https://en.wikipedia.org/wiki/Directorial_debut "Directorial debut"). A
  co-production of United States and United Kingdom, it features [child
  actors](https://en.wikipedia.org/wiki/Child_actor "Child actor") playing adult
  roles, with [Jodie Foster](https://en.wikipedia.org/wiki/Jodie_Foster "Jodie
  Foster"), [Scott Baio](https://en.wikipedia.org/wiki/Scott_Baio "Scott Baio"),
  [John Cassisi](https://en.wikipedia.org/wiki/John_Cassisi "John Cassisi"), and
  [Martin Lev](https://en.wikipedia.org/wiki/Martin_Lev "Martin Lev") in major
  roles. The film tells the story of the rise of "Bugsy Malone" and the battle
  for power between "Fat Sam" and "Dandy Dan".


  Set in New York City, it is a gangster movie spoof, substituting machine guns that fire gobs of whipped cream instead of bullets. The film is based loosely on events in New York and Chicago during [Prohibition era](https://en.wikipedia.org/wiki/Prohibition_in_the_United_States "Prohibition in the United States"), specifically the exploits of real-life gangsters such as [Al Capone](https://en.wikipedia.org/wiki/Al_Capone "Al Capone") and [Bugs Moran](https://en.wikipedia.org/wiki/Bugs_Moran).
---
