---
layout: blog
title: Kamov Ka-26 aerobatics at Kadarkút airfield
date: 2020-07-02T11:06:51.719Z
media: https://youtu.be/ZPKCD8nC9w8
context: >-
  The [Kamov Ka-26](https://en.wikipedia.org/wiki/Kamov_Ka-26) is a very special
  construction with the [coaxial rotor
  system](https://en.wikipedia.org/wiki/Coaxial_rotors) and the two radial
  engines. 


  The chopper looks very strange and sluggish, but thats not true. This helicopter with a great pilot is able to do hair-rising maneuvers with very special flight style. 


  Hungary is lucky to have a lot of Kamov Ka-26s long ago, so we have a lot of great experienced pilots. Very big thank to Ernő Jakab to made this wonderful and sometimes almost scary demonstration flight for us. He showed us all of the special maneuvers from the Kamov Ka-26 from spraying-turns to the moving pirouette and autorotations. T


  This Kamov Ka-26 is in the registration of HA-MPJ. She is a very old bird in Hungary, cames from the Hungarian Air Service, where she was used for agricultural flights. After 1990 she was kept in pieces at LHBS until Fly-Coop Kft. bought and assemblied her again. After that she was stationed at Kaposújlak airport. Nowadays HA-MPJ is doing agricultural flights around Southwest-Hungary with Kadarkút station airfield.
---
