---
layout: blog
title: Bruce Springsteen & REM - Man on the Moon
date: 2020-06-28T18:10:15.979Z
media: https://youtu.be/BisS5JxeUW0
context: >-
  [Bruce Springsteen](https://en.wikipedia.org/wiki/Bruce_Springsteen) and
  [R.E.M.](https://en.wikipedia.org/wiki/R.E.M.) during [vote for change
  concert](https://en.wikipedia.org/wiki/Vote_for_Change) in Washington. 


  [R.E.M.](https://en.wikipedia.org/wiki/R.E.M.) was an American [rock](https://en.wikipedia.org/wiki/Rock_music "Rock music") band from [Athens, Georgia](https://en.wikipedia.org/wiki/Athens,_Georgia "Athens, Georgia"), formed in 1980 by drummer [Bill Berry](https://en.wikipedia.org/wiki/Bill_Berry "Bill Berry"), guitarist [Peter Buck](https://en.wikipedia.org/wiki/Peter_Buck "Peter Buck"), bassist [Mike Mills](https://en.wikipedia.org/wiki/Mike_Mills "Mike Mills"), and lead vocalist [Michael Stipe](https://en.wikipedia.org/wiki/Michael_Stipe "Michael Stipe").


  [Bruce Springsteen](https://en.wikipedia.org/wiki/Bruce_Springsteen) is an American singer, songwriter, and musician who is both a solo artist and the leader of the [E Street Band](https://en.wikipedia.org/wiki/E_Street_Band)
---
