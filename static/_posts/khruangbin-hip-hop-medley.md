---
layout: blog
title: Khruangbin - Hip Hop Medley
date: 2020-07-06T08:30:20.164Z
media: https://www.youtube.com/watch?v=m0CB1q8CKoU
context: >-
  [Khruangbin](https://en.wikipedia.org/wiki/Khruangbin) playing a medley of hip
  hop covers.


  November 28, 2018[, The Vic Theatre ](https://en.wikipedia.org/wiki/The_Vic_Theatre)Chicago, Illinois


  [Khruangbin](https://en.wikipedia.org/wiki/Khruangbin) is an American musical trio from [Houston](https://en.wikipedia.org/wiki/Houston "Houston"), Texas, with Laura Lee on bass, [Mark Speer](https://en.wikipedia.org/wiki/Mark_Speer "Mark Speer") on guitar, and Donald Ray "DJ" Johnson Jr. on drums. The band is known for blending global music influences, such as classic [soul](https://en.wikipedia.org/wiki/Soul_music "Soul music"), [dub](https://en.wikipedia.org/wiki/Dub_music "Dub music") and [psychedelia](https://en.wikipedia.org/wiki/Psychedelia "Psychedelia"). Their debut studio album, *[The Universe Smiles Upon You](https://en.wikipedia.org/wiki/The_Universe_Smiles_Upon_You "The Universe Smiles Upon You")* (2015), draws from the history of [Thai music](https://en.wikipedia.org/wiki/Music_of_Thailand "Music of Thailand") in the 1960s, while their second album, *[Con Todo El Mundo](https://en.wikipedia.org/wiki/Con_Todo_El_Mundo "Con Todo El Mundo")* (2018), has influences from Spain and the Middle East.
---
