---
layout: blog
title: Tennis Racket Theorem
date: 2020-06-27T14:50:24.734Z
media: https://youtu.be/1VPfZ_XzisU
context: Spinning objects have strange instabilities known as [The Dzhanibekov
  Effect or Tennis Racket
  Theorem](https://en.wikipedia.org/wiki/Tennis_racket_theorem) - this video
  offers an intuitive explanation.
---
