---
layout: blog
title: "Lianne LaHavas: NPR Music Tiny Desk Concert"
date: 2020-07-01T15:36:04.659Z
media: https://youtu.be/9HUV5a7MgS4
context: >-
  [Lianne Charlotte Barnes (born 23 August 1989), known professionally as Lianne
  La Havas](https://en.wikipedia.org/wiki/Lianne_La_Havas) is a British singer
  and songwriter. Her career began after being introduced to various musicians,
  including singer [Paloma Faith](https://en.wikipedia.org/wiki/Paloma_Faith
  "Paloma Faith"), for whom she sang backing vocals. 


  In 2010, La Havas signed to [Warner Bros. Records](https://en.wikipedia.org/wiki/Warner_Bros._Records "Warner Bros. Records"), spending two years developing her songwriting, before releasing any music. La Havas' debut studio album, *[Is Your Love Big Enough?](https://en.wikipedia.org/wiki/Is_Your_Love_Big_Enough%3F "Is Your Love Big Enough?")* (2012), was released to positive reviews from critics and earned her a nomination for the BBC's [Sound of 2012](https://en.wikipedia.org/wiki/Sound_of... "Sound of...") poll and awards for the [iTunes](https://en.wikipedia.org/wiki/ITunes "ITunes") Album of The Year 2012.
---
