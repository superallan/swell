---
layout: blog
title: C'était un rendez-vous
date: 2020-06-29T08:42:46.388Z
media: https://youtu.be/WJYOMFayruw
context: >-
  [C'était un
  rendez-vous](https://en.wikipedia.org/wiki/C%27était_un_rendez-vous) is a 1976
  French [short film](https://en.wikipedia.org/wiki/Short_film "Short film")
  directed by [Claude Lelouch](https://en.wikipedia.org/wiki/Claude_Lelouch
  "Claude Lelouch"), showing a high-speed drive through
  [Paris](https://en.wikipedia.org/wiki/Paris "Paris").\

  \

  The film shows an eight-minute drive through [Paris](https://en.wikipedia.org/wiki/Paris "Paris") during the early hours (05:30) of a Sunday morning in August (when much of Paris is on summer vacation), accompanied by sounds of a high-revving engine, gear changes and squealing tyres. It starts in a tunnel of the [Paris Périphérique](https://en.wikipedia.org/wiki/Paris_P%C3%A9riph%C3%A9rique "Paris Périphérique") at [Porte Dauphine](https://en.wikipedia.org/wiki/Porte_Dauphine "Porte Dauphine"), with an on-board view from an unseen car exiting up on a slip road to Avenue Foch. Well-known landmarks such as the [Arc de Triomphe](https://en.wikipedia.org/wiki/Arc_de_Triomphe "Arc de Triomphe"), [Opéra Garnier](https://en.wikipedia.org/wiki/Op%C3%A9ra_Garnier "Opéra Garnier"), and [Place de la Concorde](https://en.wikipedia.org/wiki/Place_de_la_Concorde "Place de la Concorde") with [its obelisk](https://en.wikipedia.org/wiki/Luxor_Obelisk "Luxor Obelisk") are passed, as well as the [Champs-Élysées](https://en.wikipedia.org/wiki/Champs-%C3%89lys%C3%A9es "Champs-Élysées"). [Pedestrians](https://en.wikipedia.org/wiki/Pedestrian "Pedestrian")are passed, [pigeons](https://en.wikipedia.org/wiki/Pigeon "Pigeon") sitting on the streets are scattered, [red lights](https://en.wikipedia.org/wiki/Stoplight "Stoplight") are ignored, one-way streets are driven up the wrong way, centre lines are crossed, and the car drives on the sidewalk to avoid a rubbish lorry. The car is never seen as the camera seems to be attached below the front bumper (judging from the relative positions of other cars, the visible headlight beam and the final shot when the car is parked in front of a kerb on [Montmartre](https://en.wikipedia.org/wiki/Montmartre "Montmartre"), with the famous [Sacré-Cœur](https://en.wikipedia.org/wiki/Basilique_du_Sacr%C3%A9-C%C5%93ur,_Paris "Basilique du Sacré-Cœur, Paris") Basilica behind, and out of shot). Here, the driver gets out and embraces a young blonde woman as bells ring in the background, with the famous backdrop of [Paris](https://en.wikipedia.org/wiki/Paris).
---
