---
layout: blog
title: Elbow - Mirrorball
date: 2020-06-28T18:45:44.497Z
media: https://youtu.be/L_Hfs3Kah-E
context: >-
  [Elbow](https://en.wikipedia.org/wiki/Elbow_(band)) are an English
  [rock](https://en.wikipedia.org/wiki/Rock_music "Rock music") band formed in
  [Bury, Greater
  Manchester](https://en.wikipedia.org/wiki/Bury,_Greater_Manchester "Bury,
  Greater Manchester"), in 1997. The band consists of [Guy
  Garvey](https://en.wikipedia.org/wiki/Guy_Garvey "Guy Garvey") (lead vocals,
  guitar), Craig Potter (keyboard, piano, backing vocals), Mark Potter (guitar,
  backing vocals) and Pete Turner (bass guitar, backing vocals). They have
  played together since 1990, adopting the name Elbow in 1997. Drummer Alex
  Reeves replaced Richard Jupp first as session performer in 2016, which he
  continues to
  be.


  The band have released eight studio albums, *[Asleep in the Back](https://en.wikipedia.org/wiki/Asleep_in_the_Back "Asleep in the Back")* (2001), *[Cast of Thousands](https://en.wikipedia.org/wiki/Cast_of_Thousands "Cast of Thousands")* (2003), *[Leaders of the Free World](https://en.wikipedia.org/wiki/Leaders_of_the_Free_World "Leaders of the Free World")* (2005), *[The Seldom Seen Kid](https://en.wikipedia.org/wiki/The_Seldom_Seen_Kid "The Seldom Seen Kid")* (2008), *[Build a Rocket Boys!](https://en.wikipedia.org/wiki/Build_a_Rocket_Boys! "Build a Rocket Boys!")*(2011), *[The Take Off and Landing of Everything](https://en.wikipedia.org/wiki/The_Take_Off_and_Landing_of_Everything "The Take Off and Landing of Everything")* (2014), *[Little Fictions](https://en.wikipedia.org/wiki/Little_Fictions "Little Fictions")* (2017), and *[Giants of All Sizes](https://en.wikipedia.org/wiki/Giants_of_All_Sizes "Giants of All Sizes")* (2019). Their studio albums, as well as their B-sides compilation *[Dead in the Boot](https://en.wikipedia.org/wiki/Dead_in_the_Boot "Dead in the Boot")*(2012), all reached the top 15 of the British album chart. Seven of their singles placed in the top 40 of the British singles chart. Their most recent album, *Giants of All Sizes*, was released on 11 October 2019.


  In 2008, Elbow won the [Mercury Music Prize](https://en.wikipedia.org/wiki/Mercury_Music_Prize "Mercury Music Prize") for their album *[The Seldom Seen Kid](https://en.wikipedia.org/wiki/The_Seldom_Seen_Kid "The Seldom Seen Kid")*, and in 2009 they won the [Brit Award](https://en.wikipedia.org/wiki/Brit_Award "Brit Award") for Best British Group. In 2012, they released "[First Steps](https://en.wikipedia.org/wiki/First_Steps_(Elbow_song) "First Steps (Elbow song)")", the [BBC](https://en.wikipedia.org/wiki/BBC "BBC") theme for the 2012 London Olympics.
---
