---
layout: blog
title: 20th Century Flicks
date: 2020-07-06T08:24:18.352Z
media: https://www.youtube.com/watch?v=7mbDttuwfCo
context: >-
  [20th Century Flicks](http://www.20thcenturyflicks.co.uk/) is the oldest video
  rental store in the World. It’s small, close-knit crew has unwittingly become
  custodians of the largest collection of DVDs and VHS tapes in the UK, and
  faces a constant struggle to adapt and survive in the age of streaming and
  downloading.


  > It’s an ode to the video shop experience and a bygone way of watching movies. With studios like Disney launching their own streaming services and joining industry kingpins such as Netflix and Hulu, we have an almost endless flow of entertainment available at the click of a button. It’s amazing to me that a little independent video store can survive the Netflix cull and even outlive Blockbuster. Drop into the shop next time you’re in Bristol for a dose of movie nostalgia, have a chat about film and go home with a VHS rarity and a bag of popcorn.\

  > - Director, Arthur Cauty
---
