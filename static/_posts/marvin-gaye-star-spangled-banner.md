---
layout: blog
title: Marvin Gaye - Star Spangled Banner
date: 2020-08-18T12:52:21.345Z
media: https://www.youtube.com/watch?v=QRvVzaQ6i8A
context: >-
  [Marvin Gaye](https://en.wikipedia.org/wiki/Marvin_Gaye) gave a
  [soul](https://en.wikipedia.org/wiki/Soul_music "Soul music")-influenced
  performance at the [1983 NBA All-Star
  Game.](https://en.wikipedia.org/wiki/1983_NBA_All-Star_Game "1983 NBA All-Star
  Game")


  "**The Star-Spangled Banner**" is the [national anthem](https://simple.wikipedia.org/wiki/National_anthem "National anthem") of the [United States](https://simple.wikipedia.org/wiki/United_States "United States"). [Francis Scott Key](https://simple.wikipedia.org/wiki/Francis_Scott_Key "Francis Scott Key") wrote the words to it in 1814 after seeing [British](https://simple.wikipedia.org/wiki/Britain "Britain") ships attacking [Fort McHenry](https://simple.wikipedia.org/w/index.php?title=Fort_McHenry&action=edit&redlink=1 "Fort McHenry (not yet started)") in [Baltimore, Maryland](https://simple.wikipedia.org/wiki/Baltimore,_Maryland "Baltimore, Maryland") during the [War of 1812](https://simple.wikipedia.org/wiki/War_of_1812 "War of 1812").


  The words are set to the music of a British drinking song called "[To Anacreon in Heaven](https://simple.wikipedia.org/wiki/To_Anacreon_in_Heaven "To Anacreon in Heaven")". The song has 4 [stanzas](https://simple.wikipedia.org/wiki/Stanza "Stanza") but only the first one is usually sung.
---
