---
layout: blog
title: Metropolis II by Chris Burden
date: 2020-07-25T07:06:55.634Z
media: https://www.youtube.com/watch?v=llacDdn5yIE
context: >-
  A short doc about a kinetic sculpture that took four years to build. We had
  the honor of spending three days in [Chris
  Burden's](https://en.wikipedia.org/wiki/Chris_Burden) studio filming this
  sculpture before it was moved to the [Los Angeles Country Museum of Art
  (LACMA)](https://en.wikipedia.org/wiki/Los_Angeles_County_Museum_of_Art) where
  it is being reinstalled. 


  The installation opened fall 2011.


  Christopher Lee Burden (April 11, 1946 – May 10, 2015) was an American artist working in [performance](https://en.wikipedia.org/wiki/Performance_Art "Performance Art"), [sculpture](https://en.wikipedia.org/wiki/Sculpture "Sculpture") and [installation art](https://en.wikipedia.org/wiki/Installation_art "Installation art"). Burden became known in the 1970s for his performance art works, including *[Shoot](https://en.wikipedia.org/wiki/Shoot_(Burden) "Shoot (Burden)")* (1971), where he arranged for a friend to shoot him in the arm with a small-caliber rifle. A prolific artist, Burden created many well-known installations, public artworks and sculptures before his death in 2015.


  The Los Angeles County Museum of Art (LACMA) is an [art museum](https://en.wikipedia.org/wiki/Art_museum "Art museum") located on [Wilshire Boulevard](https://en.wikipedia.org/wiki/Wilshire_Boulevard "Wilshire Boulevard") in the [Miracle Mile](https://en.wikipedia.org/wiki/Miracle_Mile,_Los_Angeles,_California "Miracle Mile, Los Angeles, California") vicinity of Los Angeles. LACMA is on Museum Row, adjacent to the [La Brea Tar Pits](https://en.wikipedia.org/wiki/La_Brea_Tar_Pits "La Brea Tar Pits") (George C. Page Museum).


  LACMA is the [largest](https://en.wikipedia.org/wiki/List_of_largest_art_museums "List of largest art museums") art museum in the western United States. It attracts nearly a million visitors annually. It holds more than 150,000 works spanning the history of art from ancient times to the present. In addition to [art exhibits](https://en.wikipedia.org/wiki/Art_exhibition "Art exhibition"), the museum features film and concert series.
---
