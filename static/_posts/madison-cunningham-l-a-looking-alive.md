---
layout: blog
title: Madison Cunningham - L.A. (Looking Alive)
date: 2020-07-01T15:00:47.328Z
media: https://youtu.be/2fuHS74ZARg
context: '[Madison Cunningham](https://en.wikipedia.org/wiki/Madison_Cunningham)
  (born
  1996) is
  an American singer, songwriter and guitarist. Her 2019 album, [Who Are You
  Now](https://en.wikipedia.org/wiki/Who_Are_You_Now_(album) "Who Are You Now
  (album)") was nominated for the [best Americana
  album](https://en.wikipedia.org/wiki/Grammy_Award_for_Best_Americana_Album
  "Grammy Award for Best Americana Album") in the [62nd Annual Grammy
  Awards.](https://en.wikipedia.org/wiki/62nd_Annual_Grammy_Awards "62nd Annual
  Grammy Awards") *[Rolling Stone](https://en.wikipedia.org/wiki/Rolling_Stone
  "Rolling Stone")* describes her music as "a new spin on West Coast folk-rock,
  with classical tendencies, electric guitars, jazz-school chord changes and
  alt-rock strut all living under the same roof".'
---
