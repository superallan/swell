---
layout: blog
title: Cedar Planter Boxes
date: 2020-06-29T13:10:13.644Z
media: https://youtu.be/MS4wNt_R7KU
context: "> In my latest woodworking project, I wanted to help out two friends
  of mine that own a restaurant. For this project build, they asked me to make
  some outdoor planter boxes that would help divide a new outdoor seating area.
  These outdoor Cedar planter boxes ended up being 8' long, 13\" wide and about
  2' tall. I made the planter boxes from 2\" thick Cedar. I put 2 pieces
  together to make the height for the sides of the planter boxes and I mitered
  the corners and used glue, biscuits and metal brackets to hold the planters
  together. Overall this was a great woodworking project that I really enjoyed
  building, and I hope that it will help you make your own set of outdoor
  planter boxes. Tools used in this project can be found at
  [http://www.frankmakes.com/](https://www.youtube.com/redirect?v=MS4wNt_R7KU&e\
  vent=video_description&q=http%3A%2F%2Fwww.frankmakes.com%2F&redir_token=QUFFL\
  UhqbWVvUEpPdFdyM2Zna2NqMmpYX1F3TGs5WXBKZ3xBQ3Jtc0tuMWIwWXZnc1g5dzM0eU55ZDBSaj\
  JFcDNTNnRIOEl6Rm1tNUFmTlRQSXE3S2xMeUxfRXFvOUx2dENQSlZnc09EWTk4UkdPcEdDNjFFclJ\
  3UUl6YnVIblZBQTh2dEhTV2V0RWRqVVBQTUJxakJVYUdGcw%3D%3D)"
---
