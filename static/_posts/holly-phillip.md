---
layout: blog
title: Holly & Phillip
date: 2020-06-28T09:34:23.939Z
media: https://youtu.be/PHuLy0DT_84
context: >-
  [This Morning](https://en.wikipedia.org/wiki/This_Morning_(TV_programme)) is a
  British [daytime television](https://en.wikipedia.org/wiki/Daytime_television
  "Daytime television") programme that is broadcast on
  [ITV](https://en.wikipedia.org/wiki/ITV_(TV_network) "ITV (TV network)") in
  the [United Kingdom](https://en.wikipedia.org/wiki/United_Kingdom "United
  Kingdom"), presented by [Phillip
  Schofield](https://en.wikipedia.org/wiki/Phillip_Schofield "Phillip
  Schofield") and [Holly
  Willoughby](https://en.wikipedia.org/wiki/Holly_Willoughby)


  The show usually scores around 1 million viewers a day, a high rating for the ITV daytime schedules (6am-6pm). On 3 October 2018, the show had one of its highest ratings, when 2.7 million viewers tuned in for its 30th anniversary.
---
