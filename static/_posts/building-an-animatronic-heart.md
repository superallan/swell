---
layout: blog
title: Building an Animatronic Heart
date: 2020-06-27T14:58:11.145Z
media: https://youtu.be/JqDfKz8ias4
context: An advanced, [servo](https://en.wikipedia.org/wiki/Servomechanism)
  based [animatronic](https://en.wikipedia.org/wiki/Animatronics) heart
  mechanism using [arduino](https://en.wikipedia.org/wiki/Arduino) and [3D
  printing](https://en.wikipedia.org/wiki/3D_printing).
---
