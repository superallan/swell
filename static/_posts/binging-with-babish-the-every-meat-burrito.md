---
layout: blog
title: Binging with Babish - The Every-Meat Burrito
date: 2020-06-29T09:04:22.241Z
media: https://youtu.be/Mf4wwXM2o_M
context: '[Binging with
  Babish](https://en.wikipedia.org/wiki/Binging_with_Babish) is a
  [YouTube](https://en.wikipedia.org/wiki/YouTube "YouTube") cooking channel
  created by American filmmaker Andrew Rea (born September 2, 1987), alias
  Oliver Babish, which recreates recipes featured in film, television, and
  video games.'
---
