---
layout: blog
title: Bringing BACK The iPhone Headphone Jack - in China
date: 2020-06-27T15:01:27.337Z
media: https://youtu.be/utfbE3_uAMA
context: >-
  [Apple Inc](https://en.wikipedia.org/wiki/Apple_Inc.). is an American
  [multinational](https://en.wikipedia.org/wiki/Multinational_corporation
  "Multinational corporation") [technology
  company](https://en.wikipedia.org/wiki/Technology_company "Technology
  company") headquartered in [Cupertino,
  California](https://en.wikipedia.org/wiki/Cupertino,_California "Cupertino,
  California"), that designs, develops, and sells [consumer
  electronics](https://en.wikipedia.org/wiki/Consumer_electronics "Consumer
  electronics"), [computer software](https://en.wikipedia.org/wiki/Software
  "Software"), and [online
  services](https://en.wikipedia.org/wiki/Online_services "Online services").


  On September 7, 2016, Apple introduced the [iPhone 7](https://en.wikipedia.org/wiki/IPhone_7 "IPhone 7") and the [iPhone 7 Plus](https://en.wikipedia.org/wiki/IPhone_7 "IPhone 7"), which featured improved system and graphics performance, added water resistance, a new rear dual-camera system on the 7 Plus model, and, [controversially, removed the 3.5 mm headphone jack.](https://www.theverge.com/circuitbreaker/2016/6/21/11991302/iphone-no-headphone-jack-user-hostile-stupid)
---
