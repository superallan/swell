---
layout: blog
title: Genetic Engineering Will Change Everything Forever - CRISPR
date: 2020-07-01T08:27:53.368Z
media: https://www.youtube.com/watch?v=jAhjPd4uNFY
context: >-
  [CRISPR](https://en.wikipedia.org/wiki/CRISPR) is all the rage in [genetic
  engineering](https://en.wikipedia.org/wiki/Genetic_engineering) these days,
  and has already started to revolutionize several areas of human health and
  medicine.


  This video is an accessible introduction to the [molecular biology](https://en.wikipedia.org/wiki/Molecular_biology) that backs the process.
---
