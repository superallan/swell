---
layout: blog
title: The Killers - The Whole Of The Moon
date: 2020-06-28T18:40:52.824Z
media: https://youtu.be/k-cf_Oxo_U8
context: "[The Killers](https://en.wikipedia.org/wiki/The_Killers) covering [The
  Waterboys](https://en.wikipedia.org/wiki/The_Waterboys) at [TRNSMT
  Festival](https://en.wikipedia.org/wiki/TRNSMT), Glasgow Scotland 08/07/18"
---
