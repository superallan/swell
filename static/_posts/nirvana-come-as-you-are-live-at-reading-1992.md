---
layout: blog
title: Nirvana - Come As You Are (Live at Reading 1992)
date: 2020-06-25T14:00:57.957Z
media: https://youtu.be/NJqQf5DObtY
context: >-
  [Nirvana's](https://en.wikipedia.org/wiki/Nirvana_(band)) appearance at the
  1992 Reading Festival was the band's second time performing at the annual
  music festival, and their first since the success of their second album,
  *[Nevermind](https://en.wikipedia.org/wiki/Nevermind "Nevermind")*, elevated
  them to the position of what
  *[Pitchfork](https://en.wikipedia.org/wiki/Pitchfork_(website) "Pitchfork
  (website)")* called the "biggest" rock band in the world.


  The 30 August 1992 concert in Reading occurred shortly after the birth of [Frances Bean Cobain](https://en.wikipedia.org/wiki/Frances_Bean_Cobain "Frances Bean Cobain"), the daughter of Nirvana's vocalist and guitarist [Kurt Cobain](https://en.wikipedia.org/wiki/Kurt_Cobain "Kurt Cobain") and his wife [Courtney Love](https://en.wikipedia.org/wiki/Courtney_Love "Courtney Love"), vocalist and guitarist of the American rock band, [Hole](https://en.wikipedia.org/wiki/Hole_(band) "Hole (band)"). It was also performed amid press reports of Nirvana's possible dissolution, due to rumors of Cobain's addiction to heroin and tension within the band. Cobain discussed the rumors in the 1993 Nirvana biography *[Come As You Are: The Story of Nirvana](https://en.wikipedia.org/wiki/Come_As_You_Are:_The_Story_of_Nirvana "Come As You Are: The Story of Nirvana")* by [Michael Azerrad](https://en.wikipedia.org/wiki/Michael_Azerrad "Michael Azerrad"), calling them examples of "classic, typical English journalism. Sensationalism."


  As Nirvana's drummer [Dave Grohl](https://en.wikipedia.org/wiki/Dave_Grohl "Dave Grohl") recalled in a 2018 interview with *[Kerrang!](https://en.wikipedia.org/wiki/Kerrang! "Kerrang!")*, “I remember showing up to Reading ’92 and there being so many rumours that we weren't going to play, that we had cancelled. I walked backstage and some of my best friends in bands that were opening would see me and say, ‘What are you doing here?’ And I'd go, ‘We’re fucking headlining!’ And they’d be like, ‘You're actually going to play?!’ I didn't realise there was any question that we were going to play."
---
