# swell 🌊🌊🌊

Non-algorithmic, privacy focused, filter-bubble bursting video site.

### Running the project

```bash
npm install
npm run dev
```

Open up [localhost:3000](http://localhost:3000) and start clicking around.
